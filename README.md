<!-- #region -->
# Curso Nuevas herramientas de teledetección aplicadas a la caracterización morfodinamica de ríos

Este repo cuenta con toda la información para el cursado de **"Nuevas herramientas de teledetección aplicadas a la caracterización morfodinamica de ríos"**. En esta primera edición se dará los días Lunes y Miercoles de 6pm a 9pm (-5GMT) y los días Sabados de 9am a 12pm (-5GMT), desde el 11 al 30 de Abril.

Cualquier duda a ldominguezruben@gmail.com o contacto@redyaku.com


## Sobre el Curso

El curso  "Nuevas herramientas de teledetección aplicadas a la caracterización morfodinamica de ríos". pretende realizar un barrido de conceptos sobre las nuevas herramientas necesarias para el analisis de patrones morfometricos en grandes sistemas fluviales. Los grandes topicos que se estudiarana serán:
- Introducción al lenguaje Python 
- Nociones básicas de Sistemas Geoespcaiales (QGIS)
- Descripción de parámetros básicos de morfometría fluvial
- Análisis de señales (Fourier y Wavelets)
- Implementación del software MStaT
 
### Tareas
El curso contará con 5 tareas que se irán presentando a lo largo del curso y un Trabajo Final. Todas estas obligaciones serán evaluadas y deberán cumplimentarse para la finalización del curso.

### Sobre los docentes
El curso se desarrollará con el dictado de los docentes:
- [Lucas Dominguez Ruben](https://www.researchgate.net/profile/Lucas-Dominguez-Ruben-3)
- [Jorge Abad](https://www.researchgate.net/profile/Jorge-Abad-2)


### Dependencias 
Las siguientes son dependencias de Python necesarias a instalar para las tareas del curso:
- [Numpy](https://numpy.org/)
- [Scikit learn](https://scikit-learn.org/stable/index.html)
- [Matplotlib](https://matplotlib.org/)
- [Pandas](https://pandas.pydata.org/)
- [Scipy](https://scipy.org/)
- [GDAL](https://gdal.org/api/python.html)
- [Rasterio](https://rasterio.readthedocs.io/en/latest/quickstart.html)

#### Como descargar este repositorio???
Debemos ir a una terminal (Anaconda prompt, Jupyter-notebook o Jupyter.lab), en la misma ubicarnos en la carpeta donde queremos (usamos el comando ```cd direccion_de_carpeta```) descargar la información y luego realizamos el siguiente comando:
```
git clone https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria.git
```
<!-- #endregion -->

```python

```
