import ee
from ee_plugin import Map

# Colleción de uso (Revisar en el Catalogo de GEE) 1972-07-26T14:46:24Z–1978-01-06T00:00:00
land1 = ee.ImageCollection('LANDSAT/LM01/C01/T2')\
    .filterDate('1972-01-08', '1973-12-20')\
    .filterMetadata ('CLOUD_COVER', 'Less_Than', 50);# fechas de filtrado

# Podemos darle zoom y un nivel de zoom donde queremos ver
#Map.setCenter(-60.5,-31.5, 12);
median = land1.median() #aplicamos la media si hay mas de una imagen

# nombre de imagen
name = 'L1_1973' #nombre de imagen

#agregamos la media
Map.addLayer(median, {"bands": ['B6', 'B5', 'B4']},name)