#importamos las librerias
import ee
from ee_plugin import Map

# Agregamos mapas 1975-01-31T10:19:55Z–1982-02-03T00:00:00
land3 = ee.ImageCollection('LANDSAT/LM02/C01/T2')\
    .filterDate('1975-10-07', '1975-12-10')\
    .filterMetadata ('CLOUD_COVER', 'Less_Than', 5);# fechas de filtrado
    #.filter(ee.Filter.eq('WRS_PATH', -31.5))\
    #.filter(ee.Filter.eq('WRS_ROW',-60.5))\
    
# zoom in somewhere
#Map.setCenter(-60.5,-31.5, 12);
median = land3.median()


Map.addLayer(median, {"bands": ['B6', 'B5', 'B4']},'L2_1975')
