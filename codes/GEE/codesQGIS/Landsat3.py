#importamos la libreria
import ee
from ee_plugin import Map

# add some data to the Map March 1978–March 1983
land3 = ee.ImageCollection('LANDSAT/LM03/C01/T2')\
    .filterDate('1981-10-28', '1981-10-31') #1981-10-29
    #.filter(ee.Filter.eq('WRS_PATH', -31.5))\
    #.filter(ee.Filter.eq('WRS_ROW',-60.5))\
    
# zoom in somewhere
#Map.setCenter(-60.5,-31.5, 12);
median = land3.median()

Map.addLayer(median, {"bands": ['B6', 'B5', 'B4']},'Land3')
