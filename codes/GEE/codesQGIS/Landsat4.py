#importamos las librerias
import ee
from ee_plugin import Map

# add some data to the Map August 1982–December 1993
land4 = ee.ImageCollection('LANDSAT/LM04/C01/T1')\#colección
    .filterDate('1981-10-28', '1981-10-31')\#date
    .filterMetadata ('CLOUD_COVER', 'Less_Than', 100)#nubosidad
    #.filter(ee.Filter.eq('WRS_PATH', -31.5))\
    #.filter(ee.Filter.eq('WRS_ROW',-60.5))\
    
# zoom in somewhere
#Map.setCenter(-60.5,-31.5, 12);
median = land4.median()

Map.addLayer(median, {"bands": ['B6', 'B5', 'B4']},'Land4')
