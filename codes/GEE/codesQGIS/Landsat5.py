import ee
from ee_plugin import Map
# Data Availability January 1984–May 2012 
land5 = ee.ImageCollection('LANDSAT/LT05/C02/T1')\
        .filterDate('2000-01-01', '2000-12-30')\
        .filterMetadata ('CLOUD_COVER', 'Less_Than', 50)

# zoom in somewhere
#Map.setCenter(-60.5,-31.5, 12);
median = land5.median()

Map.addLayer(median, {"bands": ['B4', 'B3', 'B1'], 'min':0, 'max':200},'L5_2000_FC')
