#importamos la libreria
import ee
from ee_plugin import Map

# add some data to the Map
land7 = ee.ImageCollection('LANDSAT/LE07/C02/T1_L2')\#nombre de coleacción
    .filterDate('2010-01-10', '2010-12-15')\#date
    .filterMetadata ('CLOUD_COVER', 'Less_Than', 10)#nubosidad
    #.filter(ee.Filter.eq('WRS_PATH', -31.5))\
    #.filter(ee.Filter.eq('WRS_ROW',-60.5))\

# zoom in somewhere
#Map.setCenter(-60.5,-31.5, 12);
median = land7.median()
print(median)

Map.addLayer(median, {"bands": ['B3', 'B2', 'B1'],'min': 20, 'max': 100},'L7_2010')
