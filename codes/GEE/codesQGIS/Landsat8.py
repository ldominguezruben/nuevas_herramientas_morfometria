import ee
from ee_plugin import Map

# add some data to the Map
land8 = ee.ImageCollection('LANDSAT/LC08/C01/T1_SR')\
    .filterDate('2015-10-15', '2015-10-18') \
    .filterMetadata ('CLOUD_COVER', 'Less_Than', 10)
    #.filter(ee.Filter.eq('WRS_PATH', -31.5))\
    #.filter(ee.Filter.eq('WRS_ROW',-60.5))\

# zoom in somewhere
#Map.setCenter(-60.5,-31.5, 12);
#median = land8.median()
#print(median)

Map.addLayer(median, {"bands": ['B4', 'B3', 'B2']},'Land8_17-10-2015')