# Este code calcula el NDVI de Sentinel 2
import ee
from ee_plugin import Map

image = ee.ImageCollection('COPERNICUS/S2') \
        .filterDate('2021-01-01', '2021-12-30') \
        .filterMetadata ('CLOUDY_PIXEL_PERCENTAGE', 'Less_Than', 10)

image = ee.Image(image.median());

#Nombro a las bandas adecuadas 
NIR = image.select('B3')
Red = image.select('B8')

ndvisentinel = NIR.subtract(Red).divide(NIR.add(Red))

#paleta de colores
palette = [
    'FFFFFF', 'CE7E45', 'DF923D', 'F1B555', 'FCD163', '99B718', '74A901',
    '66A000', '529400', '3E8601', '207401', '056201', '004C00', '023B01',
    '012E01', '011D01', '011301']

Map.addLayer(ndvisentinel, {'palette': palette}, "NDVI Sentinel")