# Este code calcula el NDVI de Sentinel 2
import ee
from ee_plugin import Map

image = ee.ImageCollection('COPERNICUS/S2') \
        .filterDate('2016-01-01', '2016-12-30') \
        .filterMetadata ('CLOUDY_PIXEL_PERCENTAGE', 'Less_Than', 10)

image = ee.Image(image.median());

#Nombro a las bandas adecuadas 
NIR = image.select('B3')
Red = image.select('B8')

ndvisentinel = NIR.subtract(Red).divide(NIR.add(Red))

#paleta de colores
palette = [
    '0602ff', '307ef3', '30c8e2', '3ae237', 'd6e21f',
    'ffd611', 'ff8b13', 'ff500d', 'ff0000', 'de0101', 'c21301']

Map.addLayer(ndvisentinel, {'max': 1.0, 'min': 0,'palette': palette}, "NDVI Sentinel_2016")