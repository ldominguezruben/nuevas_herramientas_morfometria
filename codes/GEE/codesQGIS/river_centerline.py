import ee
from ee_extra.Algorithms import river
ee.Initialize()
# Find an image by ROI.
point = ee.Geometry.Point([-88.08, 37.47])
image = ee.ImageCollection("LANDSAT/LC08/C01/T1_SR")\
    .filterBounds(point)\
    .sort("CLOUD_COVER")\
    .first()
# Extract river width for a single image.
## Export to drive
river.rwc(image, folder="export", water_method='Jones2019')
## Return a ee.FeatureCollection
river.rwc(image, water_method='Jones2019', return_fc=True) 