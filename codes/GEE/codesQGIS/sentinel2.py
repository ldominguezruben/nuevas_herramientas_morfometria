import ee
from ee_plugin import Map

image = ee.ImageCollection('COPERNICUS/S2') \
        .filterDate('2016-01-01', '2016-12-30') \
        .filterMetadata ('CLOUDY_PIXEL_PERCENTAGE', 'Less_Than', 5)
        
image = ee.Image(image.median());

vis = {'bands': ['B8A', 'B11', 'B4'], 'min': 1500, 'max': 5500}
  
Map.addLayer(image, vis, 'S2_2016_FC')