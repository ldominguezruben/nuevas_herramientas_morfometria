import ee
from ee_plugin import Map

gsw = ee.Image('JRC/GSW1_0/GlobalSurfaceWater')

transition = gsw.select('transition')

Map.addLayer(transition)