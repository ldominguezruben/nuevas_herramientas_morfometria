# Este code descarga imagenes sentinel 2 y las agrupa con la misma composición y rango de banda
#importamos librerias
import datetime
from ee_plugin import Map
import ee

#creammos una variable de visualización 
vis = {'bands': ['B12', 'B8', 'B4'], 'min': 500, 'max': 5000}

#función para el seteo de las fechas
def set_time(image):
    return image.set('time', image.date().format('YYYY-MM-dd'))
    
count = 60

images = ee.ImageCollection('COPERNICUS/S2') \
    .filterDate('2019-01-01', '2019-02-01').filterBounds(Map.getCenter()) \
    .map(set_time).distinct(['time']).limit(count)
    
images = ee.ImageCollection(images)

def add_image(t):
    image = images.filter(ee.Filter.eq('system:time_start', t)).first()
    name = datetime.datetime.fromtimestamp(float(t/1000)).strftime('%Y-%m-%d')
    Map.addLayer(image, vis, name)

times = ee.List(images.aggregate_array('system:time_start')).getInfo()

#agregamos la imagen
for t in times:
    add_image(t)
