import ee
from ee_plugin import Map

def on_center_changed(center):
    print(center)

    # add center as a layer (EE API)
    pt = ee.Geometry.Point([center['lon'], center['lat']])
    Map.addLayer(pt, { 'color': 'red' }, 'center')

Map.onChangeCenter(on_center_changed)
