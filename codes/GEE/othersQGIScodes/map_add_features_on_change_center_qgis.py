import ee
from ee_plugin import Map

def on_center_changed(center):
    print(center)
    
    # add center as a layer (QGIS API)
    
    # get existing layer
    layers = QgsProject.instance().mapLayersByName('center')

    if len(layers) == 0:
        layer = QgsVectorLayer("Point", "center", "memory")
        QgsProject.instance().addMapLayer(layer)
    else:
        layer = layers[0]

    # add new feature
    provider = layer.dataProvider()
    f = QgsFeature()
    x, y = center['lon'], center['lat']
    f.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(x, y)))
    provider.addFeature(f)
    layer.updateExtents()

Map.onChangeCenter(on_center_changed)
