# Importamos las librerias necesarias
import ee
from ee_plugin import Map

# Agregamos un mapa DEM o ALOS
dem = ee.Image("AHN/AHN2_05M_RUW")
Map.addLayer(dem, {'min': -5, 'max': 50, 'palette': ['000000', 'ffffff'] }, 'DEM', True)

# Hacemos un zoom de la zona que necesitamos (Opcional)
Map.setCenter(4.4585, 52.0774, 15)

# Extraemos los limites que marcamos
bounds = Map.getBounds(True)

# Agregamos los limites que hemos visto al mapa
Map.addLayer(bounds, { 'color': 'green' }, 'bounds', True, 0.5)
