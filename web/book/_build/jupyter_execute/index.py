#!/usr/bin/env python
# coding: utf-8

# # Nuevas herramientas de teledetección aplicadas a la caracterización morfodinamica de ríos
# 
# El curso tiene el objetivo de enseñar las nuevas aplicaciones que existen para el tratamiento de información satelital para la descripción de procesos fluviales. A lo largo del curso se combinaran el uso del lenguajes Python sus aplicaciones y la descripción de elementos geometricos Fluviales presentes, en particular sobre la Amazonia.
# 
# ![giphy](images/ucayali1.gif)

# ### Docentes:
# - [Lucas Dominguez Ruben](https://www.researchgate.net/profile/Lucas-Dominguez-Ruben-3)
# - [Jorge Abad](https://www.researchgate.net/profile/Jorge-Abad-2)

# * Este apunte pretende ser una guía para el alumno que curse Nuevas herramientas de teledetección aplicadas a la caracterización morfodinamica de ríos. El mismo contiene una serie de notebooks que le permiten implementar algoritmos resolutivos de tópicos que se irán desarrollando en el curso. Para más información recurrir a [Repositorio](https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria).
# 
# * Los códigos se encuentran bajo la licencia [Licencia MIT](https://opensource.org/licenses/MIT)*

# # Info General del Curso
# * El respositorio donde se alojan todos los codes es [REPOSITORIO](https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria)
# * Los días de clases serán Lunes y Miercoles de 6pm a 9pm y los Sabados de 9am a 12pm (-5GMT) 
# * Los requisitos para la finalización del curso son la aprobación de las 5 tareas y un proyecto final.
# * Abajo se encuentran los contenidos y notebooks asociados

# ## Tabla de Contenidos
# 
# 
# ### [Tema I. Introducción a Python](notebooks/NMH01.00-pythonintro.ipynb)
# 
# * [**I.1 Iniciando el camino con Python**](notebooks/NMH01.01-iniciando-el-camino-con-python.ipynb)
# * [**I.2 Paquetes y Entornos**](notebooks/NMH01.02-paquetes-y-entornos.ipynb)
# * [**I.3 Jupyter Notebook y Lab**](notebooks/NMH01.03-jupyter-notebook-lab.ipynb)
# * [**I.4 Expresiones Lógicas y Operadores**](notebooks/NMH01.04-expresiones-logicas-y-operadores.ipynb)
# * [**I.5 Estructura de Datos**](notebooks/NMH01.05-estructura-de-datos.ipynb)
# * [**I.6 Funciones**](notebooks/NMH01.06-funciones.ipynb)
# * [**I.7 Condicionantes**](notebooks/NMH01.07-condicionantes.ipynb)
# * [**I.8 Iteraciones. For y While**](notebooks/NMH01.08-iteraciones.ipynb)
# * [**I.9 Calculamos?. Librería de computo Científico**](notebooks/NMH01.09-calculamos.ipynb)
# * [**I.10 Ploteo de datos 2D y 3D**](notebooks/NMH01.10-plott2D-3D.ipynb)
# * [**I.11 Lectura de datos**](notebooks/NMH01.11-read-data.ipynb)
# 
# 

# ### [Tema II. Introducción al tratamiento de Información Geoespacial ](notebooks/NMH02.00-intro-geoespacial.ipynb)
# 
# * II.1 Fuentes de Información Satelital (presentación)
#     * II.1.1 [Google Earth Engine](https://earthengine.google.com/)
#     * II.1.2 [Earth Explorer](https://earthexplorer.usgs.gov/)
#     * II.1.3 [ESA](https://www.esa.int/)
#     * II.1.4 [CBers](http://www2.dgi.inpe.br/catalogo/explore)
# * II.2 [**Software QGIS**](notebooks/NMH02.01-sobre-qgis.ipynb)
#     * II.2.1 [**Ejercicio 1**](notebooks/NMH02.02.02-ejercicio1.ipynb)
# * II.3 [**Python e Información Geoespacial**](notebooks/NMH02.04-python-geoespacial.ipynb)
#     * II.3.1 [**Ejercicio 2**](notebooks/NMH02.04.01-ejercicio2.ipynb)
# 

# ### [Tema III. Morfometría Fluvial](notebooks/NMH03.00-geometria-fluvial.ipynb)
# 
# * III.1 Conceptos básicos de geometría fluvial. Cauces meandriformes

# ### [Tema IV. Aplicaciones para el tratamiento de imágenes satelitales](notebooks/NMH04.00-aplicaciones.ipynb)
# 
# * IV.1 [Tratamiento de imágenes satelitales para morfometría fluvial](notebooks/NMH04.01-workflow.ipynb)
# * IV.2 Aplicaciones utiles para el curso:
#     * IV.2.1 [GEEMAP](https://geemap.org/)
#         * IV.2.1.1 [Introducción a Geemap](notebooks/NMH04.02.01.01.geemap.ipynb)
#         * IV.2.1.2 [Generación de timelapsed](notebooks/NMH04.02.01.02.geemap-timelapsed.ipynb)
#         * IV.2.1.3 [Tratamiento NDWI. Calculo de superficie de agua expuesta](notebooks/NMH04.02.01.03.geemap-NDWI.ipynb)
#     * IV.2.2 [GEE integrado en QGIS](https://gee-community.github.io/qgis-earthengine-plugin/)
# * IV.3 [Planimetría de cauces meandriformes](notebooks/NMH04.03-parametros-geometricos.ipynb)
#     * IV.3.1 [RivWidthCloud](notebooks/NMH04.03.01.riv_width_cloud.ipynb)
#     * IV.3.2 [Filtro de lineas centrales](notebooks/NMH04.03.02.centerline_filter.ipynb)
# 

# ### [Tema V. Sobre Señales](notebooks/NMH05.00-seniales.ipynb)
# * V.1 [Nociones de Fourier](notebooks/NMH05.01-sobre-fourier.ipynb)
# * V.2 [Nociones de Wavelets](notebooks/NMH05.02-sobre-wavelets.ipynb)

# ### [TEMA VI. Meander Statistics Toolbox (MStaT)](notebooks/NMH06.00-mstat.ipynb)
# * VI.1 [Sobre MStaT. Artículo Científico](https://www.sciencedirect.com/science/article/pii/S2352711021000194)
# * VI.2 [Blog de MStaT](http://meanderstatistics.blogspot.com/)
# * VI.3 [Archivos de linea central ejemplos](https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria/-/tree/main/web/book/notebooks/data/mstat/examples)
# * VI.4 [Recomendaciones de implementación y ejecución de MStaT](notebooks/NMH06.04-recomendaciones-mstat.ipynb)

# ### TEMA VII. Prognosis de cauces meandriformes
# * VII.1 [**RVR Meander Py**](notebooks/NMH07.01-rvr_meanderpy.ipynb)
# 

# ### Miscellaneous
# * [Sobre git](notebooks/NMH_A_01-git.ipynb)
# * [Sobre ambientes de trabajo](notebooks/NMH_A_02-entornos.ipynb)
# * [Sobre como proyectar coordenadas](notebooks/NMH_A_03-projections.ipynb)
# * Otros desarrollos interesantes:
#     * [Pyriverbed](notebooks/NMH07.02-pyriverbed.ipynb)
#     * [PyRis](https://github.com/fmonegaglia/pyris)
#     * [Rivamap](https://github.com/isikdogan/rivamap)

# # Tareas
# * [Tarea 1](notebooks/TPS_TF/NMHTP1.ipynb)
# * [Tarea 2](notebooks/TPS_TF/NMHTP2.ipynb)
# 
# ...

# In[ ]:




