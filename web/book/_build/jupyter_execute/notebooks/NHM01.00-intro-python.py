#!/usr/bin/env python
# coding: utf-8

# # TEMA I. Introducción a Python

# ---
# * [**I.1 Iniciando el camino con Python**](NHM01_iniciando-python.ipynb)
# * [**I.2 Paquetes y Entornos**](NMH01.02-paquetes-y-entornos.ipynb)
# * [**I.3 Jupyter Notebook y Lab**](NMH01.03-jupyter-notebook-lab.ipynb)
# * [**I.4 Expresiones Lógicas y Operadores**](NMH01.04-expresiones-logicas-y-operadores.ipynb)
# * [**I.5 Estructura de Datos**](NMH01.05-estructura-de-datos.ipynb)
# * [**I.6 Funciones**](NMh01.06-funciones.ipynb)
# * [**I.7 Condicionantes**](NMH01.07-condicionantes.ipynb)
# * [**I.8 Iteraciones. For y While**](NMH01.08-iteraciones.ipynb)
# * [**I.9 Calculamos?. Librería de computo Científico**](NMH01.09-calculamos.ipynb)
# * [**I.10 Ploteo de datos 2D y 3D**](NMH01.10-plott2D-3D.ipynb)
# * [**I.11 Lectura de datos**](NMH01.11-read-data.ipynb)

# In[ ]:




