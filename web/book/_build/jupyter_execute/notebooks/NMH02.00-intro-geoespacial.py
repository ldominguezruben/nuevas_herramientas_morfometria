#!/usr/bin/env python
# coding: utf-8

# # TEMA II. Información Geoespacial. Tratamiento de datos

# ## Motivación
# Este tema tiene el objetivo de presenta la herramienta para el manejo de información Geoespacial. El objetivo es usar la misma como software primario. Se recomienda revisar la pagina web [QGIS](https://www.qgis.org/en/site/) para el manejo de datos geoespaciales. Se implementarán códigos desarrollados en Python para el tratamiento de esta información. 
# ![QGIS](images/qgis.png)
# 

# In[ ]:




