#!/usr/bin/env python
# coding: utf-8

# # Software QGIS

# ## Instalación de QGIS
# Para el uso de QGIS es posible descargar una versión. Se recomienda la descarga de [QGIS](https://www.qgis.org/en/site/forusers/download.html) y la instalación en el el sistema operativo correspondiente. Disponible para Windows, Linux, MACOS, entre otros)  

# !TIP Recordar de descargar una versión de QGIS que sea de tipo LTR (Long Term Release), estas son versiones mas estables, o asegurarse que la versión instalada tenga el icono de Python como se indica en la siguiente imagen:
# ![qgis_python](images/qgis_python.png)

# ## Tutorial de QGIS
# Para conocer los pasos escenciales para la ejecución de QGIS puede revisar el siguiente link [Tutotial QGIS](https://www.qgistutorials.com/es/)

# ## El primer proyecto de QGIS
# Si ya hemos instalado el QGIS podemos abrirlo y ejecturalo por primera vez, para ellos debemos seguir la siguiente secuencia
# * Abrir QGIS
# * Creamos un nuevo Proyecto (New Empty Project). Nota: esto aparece en la pantalla inciial
# * Se verifica las propiedades del proyecto (Project Properties)
#     * Vamos al menu bar al Project>Properties y vemos que se encuentre seteado como Sistema de Referencia de Coordenadas (CRS, por sus siglas en ingles) que habitualmente usamos (esto depende de la región de analisis. Ver [EPSG](https://epsg.io/)) o un sistema georeferenciado mundial **EPSG:4326**
# * Guardamos el proyecto

# ## Cosas interesantes de QGIS
# ### Panels, Toolbox y Plugins
# Algo muy interesante que tiene QGIS es el panel (menu bar), el toolbox y los plugins, los cuales pueden ser modificados y a los que se puede incorporar nuevos desarrollos
# ![qgis_panel](images/qgis_panel.png)
# Otra herramienta muy interesante es el toolbox que cuenta QGIS. Para abrir la funcionalidad ir a **Processing>Toolbox**
# ![qgis_toolbox](images/qgis_toolbox.png)

# ### Respecto al plugins que usaremos en este curso
# Usaremos el plugins de GEE para la descarga y procesamiento de información satelital. La instalación del plugins se hará con 2 pasos
# * 1) Descargamos el plugins de GEE para QGIS del siguiente [link](https://plugins.qgis.org/plugins/ee_plugin/). Esto va a descargar un archivo .zip en su carpeta de descarga
# * 2) Vamos a QGIS. Sobre el menubar tocamos en la solapa de **Plugins>Manage and install plugins** y dentro de esta al a solapa **Install from ZIP**. Buscamos el .zip e instalamos
# 
# Otros plugins interesantes 
# * CBERS4A Downloader
# * mmqgis
# * profile tool
# 

# ## Basemaps
# Algo super práctico que tiene QGIS son los mapas bases (Basemaps). Para el uso de los mismos hay que adicionarlos a nuestro QGIS, una vez hecho lo siguiente ya quedarán disponibles para cualuier ejecuación de QGIS.
# * 1) Ir al panel Browser (se ubica a la izquierda del panerl de QGIS)
# * 2) Buscar el Browser XYZ Tiles y generar una nueva conección (New connection, botón derecho sobre el browser). Se deben rellenar el nombre que le daremos al basemap y el URL donde extraera la información del mapa. Ver imagen siguiente
# ![qgis_newconnection](images/qgis_newconnection.png)
# 
# Ademas de usar el basemap de Google Earth es posible usar otros mapas de los siguiente URLs
# * OpenStreet Map URL = http://tile.openstreetmap.org/{z}/{x}/{y}.png
# * ESRI World URL =https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}
# * ESRI topo URL = https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}
