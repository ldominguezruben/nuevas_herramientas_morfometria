#!/usr/bin/env python
# coding: utf-8

# # Ejercitación 1 
# El objetivo del ejercicio es iniciar al alumno en el manejo de la interfaz QGIS. Los modos de operación y funciones básicas. El tiempo estimado es de 30min.
# 
# * 1) Creamos un archivo shapefile
#     * 1.1) Agregamos un Basemap
#     * 1.2) Creamos un punto (Point Shapefile)
#     * 1.3) Creamos una linea (Line Shapefile)
#     * 1.4) Creamos una poligono (Polygon Shapefile)
#     * 1.5) Hacemos un Layout (generamos un JPEG)
#         * 1.5.1) Agregamos imagen y todo lo creado anteriormente
#         * 1.5.2) Agregamos Texto, Norte y Escala
#         * 1.5.3) Agregamos grilla

# In[ ]:




