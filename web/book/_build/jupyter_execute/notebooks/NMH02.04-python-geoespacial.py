#!/usr/bin/env python
# coding: utf-8

# # Python e Información Geoespacial
# Uno de los grandes beneficios que incorporó Python como lenguaje fue el manejo de información geoespacial. En este notebook expondremos cuales son las librerías mas habituales para el manejo de datos y como implementar parcialmente lo que hicimos en el ejercicio 1, pero en este caso con códigos Python

# ## Las librerías relevantes
# Debemos estar seguro que contamos con las siguientes librerias, las cuales son referentes de la información geoespacial

# * GDAL (hoy osgeo) ```conda install -c conda-forge gdal```
# * Rasterio ```conda install -c conda-forge rasterio```
# * Geopandas ```conda install -c conda-forge geopandas```
# * Pylandsat ```pip install pylandsat```
# * Cartopy ```conda install -c conda-forge cartopy```
# * matplotlib_scalebar ```pip install matplotlib_scale_bar```
# * Folium ```conda install -c conda-forge folium```
# * Geospatial ```conda install -c conda-forge geospatial```
# * Flusstools ```pip install flusstool```

# ## Sobre Landsatexplore
# https://pypi.org/project/landsatxplore/

# In[65]:


get_ipython().system('pip install landsatxplore')


# Dataset Name	Dataset ID
# Landsat 5 TM Collection 1 Level 1	landsat_tm_c1
# Landsat 5 TM Collection 2 Level 1	landsat_tm_c2_l1
# Landsat 5 TM Collection 2 Level 2	landsat_tm_c2_l2
# Landsat 7 ETM+ Collection 1 Level 1	landsat_etm_c1
# Landsat 7 ETM+ Collection 2 Level 1	landsat_etm_c2_l1
# Landsat 7 ETM+ Collection 2 Level 2	landsat_etm_c2_l2
# Landsat 8 Collection 1 Level 1	landsat_8_c1
# Landsat 8 Collection 2 Level 1	landsat_ot_c2_l1
# Landsat 8 Collection 2 Level 2	landsat_ot_c2_l2
# Sentinel 2A	sentinel_2a

# In[ ]:


get_ipython().system('landsatxplore search --dataset LANDSAT_TM_C1 --location 12.53 -1.53      --start 1995-01-01 --end 1995-12-31')


# ## Sobre PyLandsat
# https://pypi.org/project/pylandsat/

# In[1]:


get_ipython().system('pip install pylandsat')


# In[2]:


# Descargamos un producto entero
get_ipython().system('pylandsat download LE07_L1TP_205050_19991104_20170216_01_T1')


# In[ ]:


# Descargamos multiples imagenes
get_ipython().system('pylandsat download      LE07_L1TP_205050_19991104_20170216_01_T1      LE07_L1TP_206050_19991111_20170216_01_T1')


# In[ ]:


# Descargamos solo blue, green and red bands de un producto
get_ipython().system('pylandsat download --files B1.TIF,B2.TIF,B3.TIF      LE07_L1TP_205050_19991104_20170216_01_T1')


# In[33]:


get_ipython().system('pylandsat sync-database')


# In[64]:


get_ipython().system('pylandsat sync-database -f')


# In[35]:


get_ipython().system('pylandsat print-datadir')


# In[36]:


get_ipython().system('pylandsat search      --begin 1999 --end 2000      --path 206 --row 50      --clouds 0')


# ## PySentinel
# https://pypi.org/project/sentinel/

# In[58]:


get_ipython().system('pip install jinja2==3.0.3')
get_ipython().system('pip install sentinelhub')


# In[61]:


get_ipython().system('sentinelhub.download --help')


# In[63]:


#First import all needed modules
import sentinelhub
import pandas as pd 
import datetime

#specify tiles, for example two in the eastern part of Austria:
tiles = ["T36MZB"]

#specify start and end date (here we use May 2017)
dateStart = "2020-01-01"
dateEnd = "2020-01-31"

#create date range
dates = pd.date_range(start=dateStart, end=dateEnd)

#loop over tiles
for tile in tiles:
    print("Downloading tile: " + tile)
#loop over dates
    for date in dates:
        print(str(date.date()) + " ...")

        #try if there is a product available for the selected date
        try:
            sentinelhub.download(tile=(tile, str(date.date())), entire_product=True)
        except Exception as ex:
            template = "No image for the specified tile / date combination could be found."
            message = template.format(type(ex).__name__)
            print(message)


# # Usamos Geopandas
# A continuación se exponen algunos codigos para la visualización de datos geoespaciales

# In[1]:


#Importamos librerías geoespaciales
import geopandas as gpd
import matplotlib.pyplot as plt


# In[2]:


#Código de impresión de un mapa
path = gpd.datasets.get_path('naturalearth_lowres')
df = gpd.read_file(path)
df.plot(figsize=(12,6))


# # Ejemplo de Rasterio 
# A continuación armamos un ejemplo de ploteo con la libreria rasterio y geopandas

# In[3]:


#importamos de rasterio el show
import rasterio 
from rasterio.plot import show
from matplotlib_scalebar.scalebar import ScaleBar

#leemos la data
src = rasterio.open('data/Suelo_Agua.tif')


# In[4]:


#creamos la figura
fig, ax = plt.subplots(figsize=(15,5))

#extension de mapa
extent=[src.bounds[0], src.bounds[2], src.bounds[1], src.bounds[3]]
#ploteo del mapa
ax = rasterio.plot.show(src, extent=extent, ax=ax, cmap='pink')

# posicion de escala
scale1 = ScaleBar(
dx=1, label='Río Dulce',
    location='lower right',  # in relation to the whole plot
    label_loc='left', scale_loc='bottom'  # in relation to the line
)
# Agregamos labels
ax.grid()
ax.add_artist(scale1)
ax.plot()


# In[5]:


#Guardamos el mapa generado
fig.savefig("Ejemplo_rioDulce.tif", format='tif')


# ### Hacemos dos ejemplos mas con ploteo en otras librerias

# **Opcional** EJEMPLO Express de Plotly

# In[6]:


# El siguiente code es opcional para visualizar plotly + geopandas
import plotly.express as px
import geopandas as gpd

df = px.data.election()
geo_df = gpd.GeoDataFrame.from_features(
    px.data.election_geojson()["features"]
).merge(df, on="district").set_index("district")

fig = px.choropleth_mapbox(geo_df,
                           geojson=geo_df.geometry,
                           locations=geo_df.index,
                           center={"lat": -6, "lon": -75},
                           mapbox_style="open-street-map",
                           zoom=8.5)
fig.show()


# In[14]:


import folium
# Stamen Terrain
map = folium.Map(location = [-6,-75], tiles = "Stamen terrain", zoom_start = 9)
map


# In[12]:




