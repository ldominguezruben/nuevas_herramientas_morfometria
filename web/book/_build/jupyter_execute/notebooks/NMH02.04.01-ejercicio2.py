#!/usr/bin/env python
# coding: utf-8

# # Ejercitación 2
# El siguiente ejercicio tiene el objetivo que el alumno maneja los metodos para el tratamiento de la información satelital, pueda así ubicarse en la región y sistema de referencia, entre otras cosas. El tiempo estimado para este ejercicio es de 45min.
# 

# !NOTA: Debemos descargar del siguiente [link](https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria/-/tree/main/codes/GEE/codesQGIS) con los códigos en python necesario para la ejecución del siguiente ejercicio. A continuación dejamos un ejemplo de uno de los códigos

# In[ ]:


# Code para insertar imagenes Sentinel 2
## BLOQUE 1
import ee #importamos la libreía ee
from ee_plugin import Map #llamamos a la función Map

#BLOQUE 2
image = ee.ImageCollection('COPERNICUS/S2') \# llamamos a la mision copernicus
        .filterDate('2021-01-01', '2021-12-30') \ #fechas en las cuales podemos obtener la imagen 
        .filterMetadata ('CLOUDY_PIXEL_PERCENTAGE', 'Less_Than', 10) #nubosidad necesaria
        
image = ee.Image(image.median());# promediamos la imagen (en el caso que sean muchas para el periodo seleccionado)

print(image) # imprimimos datos de la imagen (opcional)

vis = {'bands': ['B4', 'B3', 'B2'], 'min': 0, 'max': 4000}# componemos las bandas necesarias (este caso RGB)

#BLOQUE 3  
Map.addLayer(image, vis, 'S2_2021') #agregamos y le damos un nombre a la imagen


# Consigna 
# * 2) Trabajamos con imágenes satelitales de la zona escogida
#     * 2.1) Abrimos el editor de Python en QGIS y cargamos dos códigos distintos (Ej. [Landsat7.py](https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria/-/blob/main/codes/GEE/codesQGIS/Landsat7.py); [Sentinel2.py](https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria/-/blob/main/codes/GEE/codesQGIS/sentinel2.py))
#     * 2.2) Descargamos de cada código imagenes de fechas similares
#     * 2.3) Configuramos la imagen con combinaciones de bandas (Sentinel)*
#         * 2.3.1) Combinación de bandas para _Color Natural, Falso Color, Agua-Tierra, NDVI, Sedimentos, entre otros_

# In[ ]:




