#!/usr/bin/env python
# coding: utf-8

# <h1>Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Install-geemap" data-toc-modified-id="Install-geemap-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Install geemap</a></span></li><li><span><a href="#Create-an-interactive-map" data-toc-modified-id="Create-an-interactive-map-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Create an interactive map</a></span></li><li><span><a href="#Define-region-of-interest-(ROI)" data-toc-modified-id="Define-region-of-interest-(ROI)-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Define region of interest (ROI)</a></span></li><li><span><a href="#Create-Landsat-timeseries" data-toc-modified-id="Create-Landsat-timeseries-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Create Landsat timeseries</a></span></li><li><span><a href="#Calculate-Normalized-Difference-Water-Index-(NDWI)" data-toc-modified-id="Calculate-Normalized-Difference-Water-Index-(NDWI)-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>Calculate Normalized Difference Water Index (NDWI)</a></span></li><li><span><a href="#Extract-surface-water-extent" data-toc-modified-id="Extract-surface-water-extent-6"><span class="toc-item-num">6&nbsp;&nbsp;</span>Extract surface water extent</a></span></li><li><span><a href="#Calculate-surface-water-areas" data-toc-modified-id="Calculate-surface-water-areas-7"><span class="toc-item-num">7&nbsp;&nbsp;</span>Calculate surface water areas</a></span></li><li><span><a href="#Plot-temporal-trend" data-toc-modified-id="Plot-temporal-trend-8"><span class="toc-item-num">8&nbsp;&nbsp;</span>Plot temporal trend</a></span></li></ul></div>

# ## Install geemap

# In[2]:


# Installs geemap package
import subprocess

try:
    import geemap
except ImportError:
    print('geemap package not installed. Installing ...')
    subprocess.check_call(["python", '-m', 'pip', 'install', 'geemap'])


# In[3]:


import ee
import geemap


# ## Create an interactive map

# In[2]:


Map = geemap.Map()
Map


# ## Define region of interest (ROI)

# In[9]:


roi = ee.FeatureCollection('TIGER/2018/States').filter(
    ee.Filter.eq('NAME', 'Tennessee')
)
Map.addLayer(roi, {}, "TN")
Map.centerObject(roi, 7)


# In[6]:


type(roi)


# In[8]:


#roi= Map.user_roi.getInfo()
roi = Map.user_roi
type(roi)


# ## Create Landsat timeseries

# In[7]:


images = geemap.landsat_timeseries(
    roi=roi, start_year=1984, end_year=2020, start_date='01-01', end_date='12-31'
)


# In[9]:


first = images.first()

vis_params = {'bands': ['NIR', 'Red', 'Green'], 'min': 0, 'max': 3000}

Map.addLayer(first, vis_params, 'First image')


# ## Calculate Normalized Difference Water Index (NDWI) 

# In[10]:


ndwi_images = images.map(
    lambda img: img.normalizedDifference(['Green', 'SWIR1']).rename('ndwi')
)

ndwi_palette = [
    '#ece7f2',
    '#d0d1e6',
    '#a6bddb',
    '#74a9cf',
    '#3690c0',
    '#0570b0',
    '#045a8d',
    '#023858',
]

first_ndwi = ndwi_images.first()

Map.addLayer(first_ndwi, {'palette': ndwi_palette}, 'First NDWI')


# ## Extract surface water extent

# In[11]:


water_images = ndwi_images.map(lambda img: img.gt(0).selfMask())

first_water = water_images.first()

Map.addLayer(first_water, {'palette': ['blue']}, 'First Water')


# ## Calculate surface water areas

# In[16]:


def cal_area(img):
    pixel_area = img.multiply(ee.Image.pixelArea()).divide(1e6)
    img_area = pixel_area.reduceRegion(
        **{
            'geometry': roi.geometry(),
            'reducer': ee.Reducer.sum(),
            'scale': 1000,
            'maxPixels': 1e12,
        }
    )
    return img.set({'water_area': img_area})


# In[20]:


water_areas = water_images.map(cal_area)


# In[18]:


water_stats = water_areas.aggregate_array('water_area').getInfo()
water_stats


# ## Plot temporal trend

# In[19]:


import matplotlib.pyplot as plt

x = list(range(1984, 2021))
y = [item.get('ndwi') for item in water_stats]

plt.bar(x, y, align='center', alpha=0.5)
# plt.xticks(y_pos, objects)
plt.ylabel('Area (km2)')
plt.title('Surface water dynamics in Tennessee')

plt.show()


# In[ ]:


Map.addLayerControl()
Map

