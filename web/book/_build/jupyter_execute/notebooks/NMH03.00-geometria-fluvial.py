#!/usr/bin/env python
# coding: utf-8

# # TEMA III. Morfometría fluvial

# ### MOTIVACION
# El objetivo de la sección es que el alumno distinga cuales son los parámetros geométricos básicos, que permiten caracterizar la geometria en planta de los cuaces meandriformes. Entre ellos:
# - Arcwavelength
# - Wavelength
# - Amplitud
# - Sinuosidad
# - Radio de Curvatura 
# - Otros
# 
# Se recomienda la lectura de la bibliografía dejada en el classroom.

# In[ ]:




