#!/usr/bin/env python
# coding: utf-8

# # Workflow de tratamiento de imagenes para morfometría
# 
# La motivación de este notebook es tratar de que el alumno pueda descargar la imagen y extraer la información inicial para describir la morfometría de cauces meandriformes.

# ![pythonpack](images/secuence.png)

# In[5]:


#Descargamos una imagen de una zona en particular. En este caso de la mision Landsat

# La linea siguiente tiene un codigo que permite descarga una imagen Landsat en particular (solo bandas 1, 2 y 3)
get_ipython().system('pylandsat download --files B1.TIF,B2.TIF,B3.TIF LC08_L1TP_007064_20210825_20210901_01_T1')

# Para saber el código de una imagen en particular se puede acceder a Earth explorer y buscarla o en GEE


# In[4]:


import RivWidthCloud_Python 

get_ipython().system('python rwc_landsat_one_image.py LC08_L1TP_022034_20130422_20170310_01_T1 -f csv')


# In[6]:


get_ipython().system('pip install geemap')


# In[7]:


import ee
import geemap
from geemap.algorithms import river


# In[8]:


Map = geemap.Map()
Map


# Generamos un punto de control

# In[ ]:


point = ee.Geometry.Point([ -73.831287, -5.028182])


# Filtramos la imagen que necesitamos

# In[ ]:


image = (
    ee.ImageCollection("LANDSAT/LC08/C01/T1_SR")
    .filterBounds(point)
    .sort("CLOUD_COVER")
    .first()
)


# In[ ]:


#agregamos al mapa de trabajo
Map.addLayer(image, {'min': 0, 'max': 3000, 'bands': ['B5', 'B4', 'B3']}, "Landsat")
Map.centerObject(image)


# Aplicamos el code de RiverwidthCloud

# In[ ]:


river.rwc(image, folder='export', water_method='Jones2019')


# Extraemos una imagen y la pasamos una matriz de datos numpy

# In[ ]:


import ee
import geemap
import numpy as np
import matplotlib.pyplot as plt

img = ee.Image('LANDSAT/LC08/C01/T1_SR/LC08_038029_20180810').select(['B4', 'B5', 'B6'])

aoi = ee.Geometry.Polygon(
    [[[-110.8, 44.7], [-110.8, 44.6], [-110.6, 44.6], [-110.6, 44.7]]], None, False
)

rgb_img = geemap.ee_to_numpy(img, region=aoi)
print(rgb_img.shape)


# In[ ]:


# Scale the data to [0, 255] to show as an RGB image.
# Adapted from https://bit.ly/2XlmQY8. Credits to Justin Braaten
rgb_img_test = (255 * ((rgb_img[:, :, 0:3] - 100) / 3500)).astype('uint8')
plt.imshow(rgb_img_test)
plt.show()


# Proyecto [Global River Width Dataset](https://samapriya.github.io/awesome-gee-community-datasets/projects/grwl)

# In[ ]:


water_mask = ee.ImageCollection(
    "projects/sat-io/open-datasets/GRWL/water_mask_v01_01"
).median()
Map.addLayer(water_mask, {'min': 11, 'max': 125, 'palette': 'blue'}, 'GRWL Water Mask')


# In[ ]:




