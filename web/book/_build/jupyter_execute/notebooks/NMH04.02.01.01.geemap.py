#!/usr/bin/env python
# coding: utf-8

# # Implementación de GEEMAP
# Motivación: Geemap es un desarrollo que interactua con GEE, la particularida que tiene es que desarrollado en python y fácil de manejar (tiene muchos tutoriales generados en notebooks). Además tiene mas tutoriales que videos de como hacer un pan de masa madre.
# 
# Para mas información ver:
# 
# GitHub repo: https://github.com/giswqs/geemap \
# Documentación: https://geemap.org \
# PyPI: https://pypi.org/project/geemap \
# Conda-forge: https://anaconda.org/conda-forge/geemap \
# 360+ GEE notebook examples: https://github.com/giswqs/earthengine-py-notebooks \
# Tutoriales de GEE en YouTube: https://www.youtube.com/c/QiushengWu

# Haremos un par de pasos, desde la instalación hasta la aplicación en 3 objetivos: 
# 
# [1] Instalación de geemap
# 
# [2] Descargar una imágen satelital (por ej: Landsat)
# 
# [3] Descargar un grupo de imágenes satelitales (por ej:Sentine2, promediadas)
# 
# [4] Agregamos máscaras de proyectos ya existentes

# ## Instalación de GEEMAP
# Para instalar debemos tipear la siguiente sentencia. Normalmente necesitemos ademas instalar la librería ```ee``` 

# In[1]:


#!pip install geemap
#!pip install ee


# Para verificar si instalamos ok ejecutemos la siguiente celda:

# In[52]:


#imposrtamos las librerías
import ee
import geemap
import os


# In[53]:


#generamos el mapa
Map = geemap.Map()
Map


# In[54]:


# las siguientes lineas sirve para cortar en la zona donde se haya seleccionado en el mapa
feature = Map.draw_last_feature
roi = feature.geometry()#convierte el feature en geoemtría


# ## Descarga de imagen usando ee.Image

# In[64]:


#celda básica para la descarga de una imagen
image = ee.Image('LANDSAT/LE7_TOA_5YEAR/1999_2003')#imagen media de landsat7

landsat_vis = {'bands': ['B4', 'B3', 'B2'], 'gamma': 1.4}# composición de la imagen
Map.addLayer(image, landsat_vis, "LE7_TOA_5YEAR/1999_2003", True, 0.7)# presentacion de la imagen en el mapa


# In[65]:


#busca a donde guardar la información. Selecciona la carpeta descarga
out_dir = os.path.join(os.path.expanduser('~'), 'Downloads')#busca en su computadora la ubicación de Downloads
filename = os.path.join(out_dir, 'landsat7.tif')#guarda la imagen


# ### Exporta todas las bandas como una imagen

# In[6]:


image = image.clip(roi).unmask()
geemap.ee_export_image(
    image, filename=filename, scale=90, region=roi, file_per_band=False
)


# #### Opcional: Exporta cada banda

# In[12]:


geemap.ee_export_image(
    image, filename=filename, scale=90, region=roi, file_per_band=True
)


# ## Descarga de colección de imágenes ee.ImageCollection

# In[71]:


collection = (
    ee.ImageCollection('COPERNICUS/S2')#Colección seleccionada
    .filterBounds(roi)
    .filterDate('2020-12-01', '2021-01-01')
    .filterMetadata('CLOUDY_PIXEL_PERCENTAGE', 'less_than', 10)
)
landsat_vis = {'bands': ['B4', 'B3', 'B2'], 'gamma': 1.4}


# In[72]:


#esta celda nos devuelve información de las imagenes obtenidas
dates = collection.aggregate_array('system:time_start').map(
    lambda d: ee.Date(d).format('YYYY-MM-dd')
)
print('Número de imágenes:',collection.size().getInfo())#imprimimos el número de imagenes
print('Fechas:',dates.getInfo())#imprimimos las fechas


# In[73]:


#promediamos las imagenes selccionadas
col_median = collection.median()#promediamos las imagenes
col_median_clip = col_median.clip(roi)#cortamos las imagenes
sentinel_vis = {'bands': ['B3', 'B2', 'B1'], 'min':0,'max':4000,'gamma': 1}# composición de la imagen
Map.addLayer(col_median_clip, sentinel_vis, "Sentinel2")#ploteamos la imagen en el mapa


# In[74]:


#busca a donde guardar la información. Selecciona la carpeta descarga
out_dir = os.path.join(os.path.expanduser('~'), 'Downloads')#busca en su computadora la ubicación de Downloads
filename = os.path.join(out_dir, 'sentinel2.tif')#guarda la imagen


# In[86]:


#exportamos la imagen
geemap.ee_export_image(col_median_clip, filename=filename, scale=50, file_per_band=False)


# ## Agregamos mascaras de agua de otros proyectos 
# Para mas información de estos proyectos visitar [Proyectos](https://samapriya.github.io/awesome-gee-community-datasets/projects/osm_water/)

# In[78]:


#MASCARA de AGUA
water_mask1 = ee.ImageCollection("projects/sat-io/open-datasets/OSM_waterLayer").median()
Map.addLayer(water_mask1, {'min': 11, 'max': 125, 'palette': 'Blues_r'}, 'OSM_WATER')


# Proyecto [Global River Width Dataset](https://samapriya.github.io/awesome-gee-community-datasets/projects/grwl)

# In[80]:


## OTRA MASCARA
water_mask = ee.ImageCollection(
    "projects/sat-io/open-datasets/GRWL/water_mask_v01_01"
).median()
Map.addLayer(water_mask, {'min': 11, 'max': 125, 'palette': 'summer_r'}, 'GRWL Water Mask')


# In[ ]:




