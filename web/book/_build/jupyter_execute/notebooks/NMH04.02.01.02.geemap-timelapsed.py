#!/usr/bin/env python
# coding: utf-8

# # Creación de GIF usando timelapsed
# Motivación: este notebook explica como se debería crear un gif animado para la creación de la variación temporal de una zona de interes. 

# In[1]:


# Importamos las librerías necesarias
import os
import ee
import geemap


# In[2]:


# Abrimos el mapa de trabajo
Map = geemap.Map()
Map


# In[3]:


#Leemos la figura seleccionada
roi = Map.draw_last_feature


# In[10]:


#llamamos a la función landsat_timseries para que seleccione todas las imagenes en el periodo que se selecciona
collection = geemap.landsat_timeseries(
    roi=roi, start_year=1985, end_year=2022, start_date='06-10', end_date='07-13'
)


# In[13]:


#imprimimos el número de imagenes que obtuvimos
print('Número de imágenes obtenidas:',collection.size().getInfo())


# In[6]:


#agregamos al mapa la primera imagen de la secuencia. Esto sirve para ver si existen imagenes
first_image = collection.first()

vis = {'bands': ['NIR', 'Red', 'Green'], 'min': 0, 'max': 4000, 'gamma': [1, 1, 1]}

Map.addLayer(first_image, vis, 'First image')


# ## Descargamos la ImageCollection como GIF

# In[7]:


# Definimos los argumentos para la función de animación
video_args = {
    'dimensions': 768,
    'region': roi,
    'framesPerSecond': 10,
    'bands': ['NIR', 'Red', 'Green'],
    'min': 0,
    'max': 4000,
    'gamma': [1, 1, 1],
}


# In[19]:


#seleccionamos donde vamos a guardar las imagenes
out_dir = os.path.join(os.path.expanduser("~"), 'Downloads')


# In[20]:


#generamos y descargamos el video
geemap.download_ee_video(collection, video_args, out_gif)


# ## Agregamos texto a la animación GIF

# In[10]:


#Mostramos la animación
geemap.show_image(out_gif)


# In[11]:


#Agregamos la secuencia de texto
texted_gif = os.path.join(out_dir, "landsat_ts_text.gif")
geemap.add_text_to_gif(
    out_gif,
    texted_gif,
    xy=('3%', '5%'),
    text_sequence=1985,
    font_size=30,
    font_color='#ffffff',
    add_progress_bar=False,
)


# In[12]:


#Agregamos el titulo del texto
label = 'Zona ejemplo'
geemap.add_text_to_gif(
    texted_gif,
    texted_gif,
    xy=('2%', '88%'),
    text_sequence=label,
    font_size=30,
    duration=200,#esta linea permite hacerlo mas lento al gif
    font_color='#ffffff',
    progress_bar_color='cyan',
)


# In[13]:


#mostramos el video final
geemap.show_image(texted_gif)


# In[ ]:


#Guardamos el gif generado

