#!/usr/bin/env python
# coding: utf-8

# # Calculo de Normalized Difference Water Index (NDWI)
# 
# Este indice permite obtener los cambios relacionado al contenido de agua de los cuerpos. Para mas información ver [McFeeters (1996)](https://www.tandfonline.com/doi/abs/10.1080/01431169608948714). La ecuación que representa al indice es la siguiente, aunque dependiendo de la mision las bandas correspondientes a green y Nir pueden variar de numeración. Ejemplo Sentinel 2 B3 (Banda Verde) y B8 (Banda NIR) o Landsat 8 B3 (Banda verde) y B5 (Banda NIR).
# 
# $$NDWI=\frac{B_{green}-B_{NIR}}{B_{green}+B_{NIR}}$$ 

# **Objetivo:**
# En este notebook seleccionaremos una area de interes y calcularemos como varia la superficie expuesta de agua de dicha zona.

# ## Trabajamos sobre una zona de interes

# In[2]:


# Importamos las librerías necesarias
import os
import ee
import geemap


# In[3]:


Map = geemap.Map()
Map


# Se define la zona de interes en el mapa previo.

# In[74]:


#zona de interes
feature = Map.draw_last_feature
roi = feature.geometry()


# In[75]:


#parametros de filtrado de las imágenes satelitales
start_date = '2015-01-01'#fecha de inicio 
end_date = '2022-05-06'#fecha final
cloud_threshold = 0.05#porcentaje de nubes
ndwi_threshold = 0


# In[76]:


#aplicamos losfiltros indicados previamente
images = (
    ee.ImageCollection('LANDSAT/LC08/C01/T1_SR')
    .filterBounds(roi)
    .filterDate(start_date, end_date)
    .filterMetadata('CLOUD_COVER', 'less_than', cloud_threshold)
)


# In[77]:


#imprimimos cuantas imagenes tenemos
print(images.size().getInfo())


# In[78]:


#imprimimos información de las imagenes
print(images.aggregate_array('system:id').getInfo())


# In[79]:


#imprimimos las fechas de las imagenes que obtuvimos
dates = images.aggregate_array('system:time_start').map(
    lambda d: ee.Date(d).format('YYYY-MM-dd')
)
print(dates.getInfo())


# In[80]:


#creamos una función 
def extract_water(img):
    '''La entrada es la secuencia de imágenes que hemos extraído previamente.
    La salida las imagenes seleccionadas con el NDWI limite'''
    ndwi_image = img.normalizedDifference(['B3', 'B5'])
    water_image = ndwi_image.gt(ndwi_threshold)
    return water_image


# In[81]:


#aplicamos la función previa y extraemos el NDWI
ndwi_images = images.map(extract_water)


# In[82]:


#imprimimos la ocurrence de agua
occurrence = ndwi_images.sum().selfMask()
Map.addLayer(occurrence.randomVisualizer(), {}, 'Water Occurrence')


# In[86]:


#pasamos la info de raster a vector
def ras_to_vec(img):
    vec = img.selfMask().reduceToVectors(scale=30, maxPixels=70000000)
    vec = vec.filterBounds(roi)
    return vec.set({'area': vec.geometry().area(1).divide(1e6).round()})


# In[87]:


#convertimos de raster a vector
ndwi_vectors = ndwi_images.map(ras_to_vec)


# In[88]:


#calculamos el area
areas = ndwi_vectors.aggregate_array('area')
print(areas.getInfo())


# In[89]:


#ploteamos la variación de la superficie expuesta
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(12, 6))

x = dates.getInfo()#seteamos el eje x
y = areas.getInfo()#seteamos el eje y

plt.plot(x, y, marker='o')
plt.xlabel('Fecha')
plt.ylabel('Superficie de agua expuesta [km2]')
plt.grid()
plt.show()
#plt.savefig('figure_NDWI.tif', dpi=100)


# In[ ]:




