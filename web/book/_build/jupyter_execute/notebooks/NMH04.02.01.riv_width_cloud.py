#!/usr/bin/env python
# coding: utf-8

# # RivWidthCloud
# Repo:https://github.com/seanyx/RivWidthCloudPaper

# In[1]:


get_ipython().system('git clone https://github.com/seanyx/RivWidthCloudPaper')


# ### Paper cientifico
# https://ieeexplore.ieee.org/document/8752013

# In[3]:


# show the help message
get_ipython().system('python RivWidthCloudPaper/RivWidthCloud_Python/rwc_landsat_one_image.py -h')


# In[2]:


get_ipython().system('python RivWidthCloudPaper/RivWidthCloud_Python/rwc_landsat_one_image.py LC08_L1TP_022034_20130422_20170310_01_T1 -f csv')


# In[ ]:




