#!/usr/bin/env python
# coding: utf-8

# # RivWidthCloud
# Este repositorio sirve para obtener la linea central y ancho de un cauce, tomando una imagen o batch de imagenes satelitales. ACLARACIÖN: Solo funciona con Landsat 5, 7 y 8.
# 
# Repo:https://github.com/seanyx/RivWidthCloudPaper
# Paper: https://ieeexplore.ieee.org/document/87520

# ## Clonamos el repositorio

# In[1]:


#!git clone https://github.com/seanyx/RivWidthCloudPaper


# In[3]:


# show the help message
#!python RivWidthCloudPaper/RivWidthCloud_Python/rwc_landsat_one_image.py -h


# ## Utilizamos geemap para saber si hay información 

# In[10]:


#importamos las librerias
import geemap
import ee


# In[24]:


#mostramos el mapa
Map = geemap.Map()
Map


# In[12]:


#zona de interes
feature = Map.draw_last_feature
roi = feature.geometry()


# In[21]:


#llamamos a la colección y buscamos por los filtros
images = (
    ee.ImageCollection("LANDSAT/LC08/C01/T1_SR")
    .filterBounds(roi)
    .filterMetadata('CLOUD_COVER', 'less_than', 0.05)
)


# In[22]:


#extraemos el Id de la imagen y la zona en base a los filtro previos
ids = images.aggregate_array("LANDSAT_ID").getInfo()
ids


# ## Implementación de Rivwidthcloud

# In[25]:


#!python RivWidthCloudPaper/RivWidthCloud_Python/rwc_landsat_one_image.py LC08_L1TP_230079_20131117_20170428_01_T1 -f csv


# In[ ]:




