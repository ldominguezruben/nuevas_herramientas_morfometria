#!/usr/bin/env python
# coding: utf-8

# # Filtro de lineas centrales
# Objetivo: Este notebook pretende importar la linea central obtenida por el rivwidth y poder filtrado. Caso contrario se recomienda continuar trabajando en QGIS.
# 

# In[1]:


#importamos librerias
import pandas as pd
import numpy as np


# In[ ]:


#leemos la linea central 
centerlinexy = np.loadtxt('data/ucayali_centerline.txt')
x = centerlinexy[:, 0]
y = centerlinexy[:, 1]


# In[2]:


#leemos la librería necesaria
from codes.smooth_centerline import smooth_centerline


# In[7]:


#aplicamos el suavizado
SMOLEV = 2# smooth level
ssmo, xsmo, ysmo = smooth_centerline(x,y, SMOLEV)


# In[ ]:


#ploteamos la información obtenida

