#!/usr/bin/env python
# coding: utf-8

# # TEMA V. Teoría de Señales 
# ### Fourier y Wavelets

# ### ¿Que son las señales?
# 
# Hay muchos tipos de ondas en nuestra vida, por ejemplo, si arrojas una piedra a un estanque, puedes ver cómo se forman las ondas y viajan en el agua. Por supuesto, hay muchos más ejemplos de ondas, algunas de ellas incluso difíciles de ver, como las ondas sonoras, las ondas sísmicas, las microondas (que usamos para cocinar nuestra comida en la cocina). Pero en física, una onda es una perturbación que viaja a través del espacio y la materia con una transferencia de energía de un lugar a otro. Es importante estudiar las ondas en nuestra vida para comprender cómo se forman, viajan, etc. Antes de continuar, primero familiaricémonos con cómo modelamos las ondas y las estudiamos.

# Podemos modelar una sola onda como un campo con una función _F(x,t)_, donde x es la ubicación de un punto en el espacio, mientras que _t_ es el tiempo. Un caso más simple es la forma de un cambio de onda sinusoidal sobre _x_.

# In[1]:


#EMPEZAMOS CON PYTHON
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 30, 401)#creamos hasta 30 401 puntos
y = np.sin(x) #generamos la función sinusoidal

#Iniciamos la grafica
plt.figure(figsize = (8, 4))
plt.plot(x, y, 'r')
plt.grid()
plt.ylabel('Amplitud')
plt.xlabel('Positición (x)')
plt.show()


# ### Caracteristicas geometricas de una onda

# Podemos ver que las ondas pueden ser una entidad continua tanto en el tiempo como en el espacio. Pero en realidad, muchas veces discretizamos el tiempo y el espacio en varios puntos. Por ejemplo, podemos usar sensores como acelerómetros (pueden medir la aceleración de un movimiento) en diferentes lugares de la Tierra para monitorear los terremotos, que es la discretización espacial. De igual forma, estos sensores suelen registrar los datos en determinados momentos lo que supone una discretización temporal. Para una sola onda, tiene diferentes características geométricas. 

# ![pythonpack](images/ondas.jpg)

# La **amplitud** se utiliza para describir la diferencia entre los valores máximos y el valor de referencia. Una onda sinusoidal es una señal periódica, lo que significa que se repite después de cierto tiempo, que se puede medir por período. El **periodo** de una onda es el tiempo que tarda en terminar el ciclo completo, en la figura podemos ver que el periodo se puede medir a partir de los dos picos adyacentes. La frecuencia describe el número de ondas que pasan por un lugar fijo en un tiempo determinado. La frecuencia se puede medir por cuántos ciclos pasan en 1 segundo. Por lo tanto, la unidad de frecuencia es ciclos/segundo, o hertz (abreviado Hz), más comúnmente utilizado. La frecuencia es diferente del período, pero están relacionados entre sí. La frecuencia se refiere a la frecuencia con la que sucede algo, mientras que el período se refiere al tiempo que lleva completar algo, matemáticamente,

# $periodo =\frac{1}{frecuencia}$

# De las figura, también podemos ver que los puntos azules en las ondas sinusoidales, estos son los puntos de discretización que hicimos tanto en el tiempo como en el espacio. Por lo tanto, solo en estos puntos, hemos muestreado el valor de la onda. Por lo general, cuando registramos una onda, necesitamos especificar con qué frecuencia muestreamos la onda en el tiempo, esto se denomina muestreo. Y a esta tasa se le llama tasa de muestreo, con la unidad Hz. Por ejemplo, si muestreamos una onda a 2 Hz, significa que cada segundo muestreamos dos puntos de datos. Como entendemos más sobre los conceptos básicos de una onda, ahora veamos una onda sinusoidal con más cuidado. Una onda sinusoidal se puede representar mediante la siguiente ecuación:

# $y(t) =Asin(\omega t+\phi)$

# donde **A** es la amplitud de la onda, **ω** es la frecuencia angular, que especifica cuántos ciclos ocurren en un segundo, en radianes por segundo. $\phi$ es la fase de la señal. Si **T** es el período de la onda y **f** es la frecuencia de la onda, entonces **ω** tiene la siguiente relación con ellos:

# $\omega=\frac{2\pi}{T}=2\pi f$

# In[ ]:




