#!/usr/bin/env python
# coding: utf-8

# # Sobre wavelets
# El objetivo del notebook es enseñar al alumno implementar librerías de aalisis wavelets para entender algunos conceptos básicos de la metodología. Se indagaran en señales sintéticas conocidas a las que se las filtrará usando el analisis de señales wavelets.
# 
# Se recomienda la lectura del artículo científico [Torrece and Compo (1998)](https://psl.noaa.gov/people/gilbert.p.compo/Torrence_compo1998.pdf).

# La librería que usaremos en este caso será [Pywavelets](https://pypi.org/project/PyWavelets/):
# * ``` pip install waipy```
# * ``` pip install PyWavelets```
# * ``` pip install pycwt```
# 
# Este notebook se basó en el repositorio desarrollado por [Waipy](https://github.com/mabelcalim/waipy)

# In[43]:


# importamos la libreria mas importante para el analisis wavelets 
import waipy
import pywt
import math
import matplotlib.pyplot as plt
import pycwt as wavelet
import numpy as np
from pycwt.helpers import find
import pandas as pd
from matplotlib import pyplot


# In[44]:


Construimos una señal conocida de tipo sinoidal y cosinoidal

# creamos la señal del tiempo (las dimensiones pueden ser arbitrarias en este punto)
z = np.linspace(0,2048,2048)
# creamos seno y coseno
x = np.sin(50*np.pi*z)
y = np.cos(50*np.pi*z)

#ploteamos
plt.plot(x)
plt.plot(y)


# In[45]:


# llamamos a las librerías
get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')

#implementamos a la señal conocida 
data_norm = waipy.normalize(x)
result = waipy.cwt(data_norm, 1, 1, 0.125, 2, 4/0.125, 0.72, 6, mother='Morlet',name='x')
waipy.wavelet_plot('Sine', z, data_norm, 0.03125, result)


# In[46]:


# plicamos para coseno
data_norm1 = waipy.normalize(y)
result1 = waipy.cwt(data_norm1, 1, 1, 0.125, 2, 4/0.125, 0.72, 6, mother='Morlet',name='y')
dtmin = 0.25/8    # dt/n of suboctaves
waipy.wavelet_plot('Cosine', z, data_norm1, dtmin, result1)


# In[47]:


WPS12, coherence, phase_angle,cross_power = waipy.cross_wavelet(result['wave'], result1['wave'])
figname='example1.png'
waipy.plot_cross('Crosspower sine and cosine', cross_power, phase_angle, z, result, result1,figname)


# In[72]:


# implementamos a una señal random

x = np.linspace(0,100,100)
y1 = np.random.rand(100)  # Generation of the Random Signal 1

#normalizamos la señal
data_norm = waipy.normalize(y1)

dt =1
pad = 1         # pad the time series with zeroes (recommended)
dj = 0.25       # this will do 4 sub-octaves per octave
s0 = 2*dt       # this says start at a scale 
j1 = 7/dj       # this says do 7 powers-of-two with dj sub-octaves each
lag1 = 0.72     # lag-1 autocorrelation for red noise background
param = 6
mother = 'Morlet' #esta señal puede ser modificada
dt = 1/4.
dtmin = 0.25/8    # dt/n of suboctaves

result = waipy.cwt(data_norm, dt, pad, dj, s0, j1, lag1, param, mother='Morlet',name='x')

waipy.wavelet_plot('Signal random', x, data_norm, dtmin, result)


# In[ ]:




