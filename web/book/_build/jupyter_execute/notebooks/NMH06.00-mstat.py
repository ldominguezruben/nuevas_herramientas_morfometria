#!/usr/bin/env python
# coding: utf-8

# # TEMA VI. Meander Statistics Toolbox (MStaT)
# MOTIVACION: 
# El objetivo de esta sección es describir el funcionamiento básico del paquete de códigos Meander Statistics Toolbox [MStaT](https://www.sciencedirect.com/science/article/pii/S2352711021000194). Inicialmente se tomaran una serie de tramos de cauces a los que se implementará el desarrollo, se discutirán resultados y se evaluarán criterios de uso.
#  

# ![image.png](attachment:image.png)

# In[ ]:




