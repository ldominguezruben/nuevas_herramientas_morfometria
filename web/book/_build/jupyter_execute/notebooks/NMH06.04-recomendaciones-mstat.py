#!/usr/bin/env python
# coding: utf-8

# # Recomendaciones sobre la implementación de MStaT

# ### Sobre los datos de entrada

# Como se indicó en la clase se vieron que los parámetros de entrada para la ejecución de MStat son la **linea central** y **el ancho medio** del cauce. 
# 
# - 1) **Línea Central** Se recomienda la correcta digitalización de los mismos y en esete sentido existen una serie de aplicaciones que lo permiten como [RivWidthCloud](https://github.com/seanyx/RivWidthCloudPaper). En esta aplicación suele tener algunos inconvenientes en la digitalizació (dependiendo de las nubes entre otras cosas), es por eso que se recomienda la visualización de los resultados de manera manual, de esta manera se pueden corregir los errores (quiebres abruptos de la señal entre otras cosas). Dado que esta herramientas obtiene la linea central de las imagenes LANDSAT su impleentación se recomienda a cauces cuyo ancho es mayor a 200-300m. La conformación del archivo de linea central debe ser con 2 columnas las cuales deben contener los puntos proyectados (sistema de referencia adoptado) (X, Y). 
# 
# - 2) Sobre el **ancho medio** una forma de obtención es la herramienta indicada anteriormente o sino la toma de muestars manuales de al menos 30 anchos de manera de eliminar incertidumbres en la toma de anchos.

# ### Sobre la instalación de MStaT

# Como se extresa en el [Blog MStat](http://meanderstatistics.blogspot.com/) existen dos formas de instalación, una mediante la descarga del aplicativo [Compilado](https://drive.google.com/drive/folders/1-M9awz3nQOGMprdT4EFlVxWWGYTSEqH3) y otra mediante el [código fuente](https://github.com/ldominguezruben/meanderstatisticstoolbox). Se recomienda inicialmente usar la versión compilada, y en caso de que ésta no funciones (incompatibilidad con MATLAB) se recomienda el uso del código fuente. **Recordar que se debe descargar la versión del Compilador de Matlab para el uso de la versión compilada (el cual es gratuito) [MCR 2015b](https://www.mathworks.com/products/compiler/matlab-runtime.html)**

# ### Sobre la implementación 

# **TIP 1** En cuanto a la elctura de datos de linea centralse recomienda que la misma etse proyectada y que se haga un analisis avanzado de la linea central de entrada. En este punto se recomienda (en caso de ser necesario) aumentar el orden del poligono de ajuste. Si esto no alcanza las mejoras necesarias se recomienda la reducción del ancho, esta ultima opción puede retardar el calculo. Si hay errores de lectura se recomienda la visualizción de la linea central en otro visualizador. **CHEQUEAR LA LINEA CENTRAL**

# **TIP 2** Se recomienda la exportación de los datos una vez que se ejecuta el código (River Statistics). La exportación de las imágenes es un buen recurso

# **TIP 3** La lectura de entrada para el Modulo de Migración se recomienda que se haga de las dos lineas centrales (t0 y t1). Recorda que se puede evaluar la lineas centrales por separado con el boton de propiedades avanzadas (para que funcione debe estar activo una de las filas de entrada de datos).

# **TIP 4** Respecto al modulo de confluencia considere necesario la diguitalización de los cauces secundarios hasta la intersección con el cauce principal. La lectura de los archivos se recomienda en un solo paso

# **Cualquier error en la implementación recuerde de contactarse con el desarrollador y reportarlo como un issue en https://github.com/ldominguezruben/meanderstatisticstoolbox o un mail a ldominguezruben@gmail.com**

# In[ ]:




