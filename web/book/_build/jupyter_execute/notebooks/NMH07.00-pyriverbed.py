#!/usr/bin/env python
# coding: utf-8

# # TEMA VII. Python River Bed (PyRiverBed)
# MOTIVACION
# El objetivo de este tema es que el alumno implemente su código PyRiverBed (modulo de migración) para la utilización del mismo para la predicción de la migración. 
# Mayores detalles se presentan en el [artículo](https://www.sciencedirect.com/science/article/pii/S0098300421000637).

# Para mayor información ir al siguiente link donde se presenta el software [YOUTUBE](https://www.youtube.com/watch?v=zjffVOxxmOI)

# La información de linea central del Ucayali 2011-2016 se encuentra en el repositorio [AQUI](https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria/-/tree/main/web/book/notebooks/data/pyriverbed/centerline)

# In[ ]:




