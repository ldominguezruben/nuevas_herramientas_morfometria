#!/usr/bin/env python
# coding: utf-8

# # RVR Meander Py
# En este notebook implementaremos el modelo RVR Meander ([Abad and Garcia, 2006](https://linkinghub.elsevier.com/retrieve/pii/S009830040500114)) para estimar la migración de cauces meandriformes. Para ello seleccionaremos un tramo de un cauces digitalizaremos la linea central para dos periodos, calibraremos el modelo e implementaremos en el período de evaluación.

# In[1]:


#Importamos las librerias para hacerlo funcionar
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pyproj import Proj, transform# projeccion
#codes from RVR
from codes.rvr_meanderpy.resample_centerline import resample_centerline
from codes.rvr_meanderpy.migrationRVR import migrationRVR
from codes.rvr_meanderpy.lag import lag
#importamos plotly
import plotly.graph_objects as go
import warnings
warnings.filterwarnings('ignore')


# ## Pasos
# ### 1 Selección del tramo a estudiar
# ### 2 Digitalización de la linea central
# ### 3 Calibración del modelo 
# ### 4 Implementación del periodo a evaluar

# ## Leamos los datos analizar

# ## 1) y 2) Selección del tramo a estudiar y digitalización de la linea central
# En este paso implementaremos la librería RivWidthCloud, para ello recomendamos la revisión del [notebook](NMH04.03.01.riv_width_cloud.ipynb) 

# [NOTA] Se recomienda en caso de no alcanzar una buena digitalizacón el uso de la la herramienta QGIS para lograrlo. \
# [NOTA 1] Se recomienda el uso de herramientas de reproyección como son expresadas en el siguiente [notebook](NMH_A_03-projections.ipynb)

# ## 3) Calibración del modelo
# A continuación se empleara la calibración de un tramo de cauce del río Ucayalí para un periodo de tiempo de 10 años. El tramo de estudio es el siguiente. Haremos la lectura de los datos de entrada, digitalización para dos periodos 2011, 2016 y 2021. 

# In[2]:


# importamos un txt con la linea central alcanzada para el primer año de evaluación. Nuestro caso 2011
df2011 = pd.read_csv('data/testcases_RVR/2011_Ucayali.txt',sep='\t', names=['X','Y'])
# importamos un txt con la linea central alcanzada para el primer año de evaluación. Nuestro caso 2016
df2016 = pd.read_csv('data/testcases_RVR/2016_Ucayali.txt',sep='\t', names=['X','Y'])


# In[3]:


# Ploteamos los datos de donde se realizo el test, en forma de linea para ver por donde se realizo el relevamiento
outProj = Proj(init='epsg:4326')#coordenadas geograficas WGS 84
inProj = Proj(init='epsg:32718')#UTM 16N


# In[4]:


#Creamos lnos campos en la base de datos
df2011["Lat"]=''
df2011["Lon"]='' 
#Creamos lnos campos en la base de datos
df2016["Lat"]=''
df2016["Lon"]='' 

#proyectamos para 2011
df2011['Lon'],df2011['Lat']= transform(inProj,outProj,df2011['X'],df2011['Y'])

#proyectamos para 2016
df2016['Lon'],df2016['Lat']= transform(inProj,outProj,df2016['X'],df2016['Y'])


# In[5]:


# ploteamos las dos lineas centrales 2011-2016
fig = go.Figure(go.Scattermapbox(
    mode = "lines",
    lon = df2011.Lon,
    lat = df2011.Lat,
    name='Ucayali2011',
    marker = {'size': 10}))


fig.add_trace(go.Scattermapbox(
    mode = "lines",
    lon = df2016.Lon,
    lat = df2016.Lat,
    name='Ucayali2016',
    marker = {'size': 10}))

fig.update_layout(
    margin ={'l':0,'t':0,'b':0,'r':0},
    mapbox = {
        'style': "stamen-terrain",
        'center': {'lon': -74.4, 'lat': -5.6},
        'zoom': 8})

fig.show()


# A continuación calibramos el modelo para un periodo de 5 años (2011-2016) para ello ajustamos el modelo en un periodo de calibración queremos obtener a partir de la linea central del 2011 una parecida a la que obtuvimos para el 2016

# In[11]:


#Parametros de entrada para correr RVR
WIDTH = 500 # ancho de río
DEPTH = 14 #profundidad
C0 = 0 #initial curvature
FR0 = 0.1 #Froude Number
CF0 = 0.05 #friction coefficient
E0 = 1e-7 #migration parameter
UB0 = 0 #initial velocity
anios = 5 #anios simulados (2001-2016)
DT = 86400 #delta time
TSTEPS = 365*anios+1#step number
GPRINT = 365 #graphic print
LPRINT = 150#listing print

INTERVAL = WIDTH/2/DEPTH #interval number


# In[12]:


#leemos los datos de entrada
x = df2011.iloc[:,0].values
y = df2011.iloc[:,1].values
#print(x)
length = x.size
s = np.zeros(length)
for j in range(1, x.size):
    s[j] = s[j-1] + np.sqrt((x[j]-x[j-1])**2 + (y[j]-y[j-1])**2)


# In[13]:


#resampleamos el archivo
s, x, y, cur, theta = resample_centerline(s, x, y, INTERVAL)


# In[14]:


#ejecutamos el code
for t in range(TSTEPS):
    migrationRVR(s, x, y, cur, cur, theta, t, WIDTH, DEPTH, UB0, C0, FR0, CF0, E0, DT, TSTEPS, GPRINT, LPRINT)
#Notar que no se considera el lag de curvas. Ver mas abajo en este notebook


# A continuación vamos a evaluar los resultados con la linea central previamente alcanzada (para el 2016)

# In[15]:


# importamos el csv correspondientes a la linea central calculada por RVR
dfRVR = pd.read_csv('1825.csv',sep=',')


# In[16]:


#Creamos lnos campos en la base de datos
dfRVR["Lat"]=''
dfRVR["Lon"]='' 
#proyectamos para RVR
dfRVR['Lon'],dfRVR['Lat']= transform(inProj,outProj,dfRVR['x'],dfRVR['y'])


# In[18]:


#ploteamos
fig = go.Figure(go.Scattermapbox(
    mode = "lines",
    lon = df2016.Lon,
    lat = df2016.Lat,
    name='Ucayali2016',
    marker = {'size': 10}))


fig.add_trace(go.Scattermapbox(
    mode = "lines",
    lon = dfRVR.Lon,
    lat = dfRVR.Lat,
    name='UcayaliRVR',
    marker = {'size': 10}))

fig.add_trace(go.Scattermapbox(
    mode = "lines",
    lon = df2011.Lon,
    lat = df2011.Lat,
    name='UcayaliRVR',
    marker = {'size': 10}))

fig.update_layout(
    margin ={'l':0,'t':0,'b':0,'r':0},
    mapbox = {
        'style': "stamen-terrain",
        'center': {'lon': -74.4, 'lat': -5.6},
        'zoom': 8})

fig.show()


# En caso de agregar el LAG se puede ejecutar el siguiente code. Para mayor inforamción ver [Zhi and Garcia, 2021](https://www.sciencedirect.com/science/article/pii/S0098300421000637?via%3Dihub)

# In[9]:


#consideramos el lag de las curvas
LAG = 3
#ejecutamos el code
for t in range(TSTEPS):
    cur_lag = lag(s, cur, t, LAG, LPRINT, WIDTH)

