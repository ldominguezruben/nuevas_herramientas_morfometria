#!/usr/bin/env python
# coding: utf-8

# 
# # Git, Gitlab y GitHub

# ## ¿Qué es git?

# Supongamos que trabajan en un proyecto (el PROYECTO TESIS)
# 
#  - Crean el directorio *Proyecto TESIS DOC* para guardar los archivos de código, recursos, etc.
# 
# Luego de un par de horas comienzan a trabar y meterle cambios. Entonces ¿Qué hacen?

#  - Modifican los códigos originales? (Con la esperanza de que el Jefe sabe lo que pide)

#  - Crean el directorio *Proyecto TESIS DOC_v21289304*? (Con la esperanza de que no les pidan 18 cambios más adelante)

# Git es un sistema de control de versiones, que les permite realizar una serie de procesos en su codigo o desarrollo:
#  - Guardar el historial de revisiones de un proyecto en un directorio
#  - Retroceder a cualquier versión del proyecto en cualquier momento
#  - Implementar nuevas características sin romper el codigo principal (Branches)
#  - Facilita la colaboración con varias personas. Permite que cada una de ellas tenga su propia versión de los archivos en su computadora.

# ## Cómo funciona?

#  - Git lleva el registro de cambios del repositorio. El repositorio es un directorio, todos sus archivos y subdirectorios.
#  - Los cambios se registran con **commits**.

# ## Configuración incial
# ----
# Veamos si lo tenemos instalado

# In[1]:


get_ipython().system('git --version  # La última versión estable es 2.28.')


# ### Nombre y Correo
# Ejemplo en el caso del profesor:
!git config --global user.name ldominguezruben
!git config --global user.email ldominguezruben@gmail.com
# ## Clonar un repositorio externo (nuestro caso)

# Abrimos el Anaconda Prompt y vamos a la carpeta donde deseamos descargar el repo externo. Recordar [Windows](https://www.lifewire.com/list-of-command-prompt-commands-4092302).
# 
# ```cd carpeta_donde_quiero_descargar```\
# ```git init``` \
# ```git clone https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria```

# ### Para bajar cambios que haya hecho el profe
# ``` git pull https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria```

# ## [Github](https://github.com/)
# 
# ![image.png](attachment:image.png)

# ## [Gitlab](https://about.gitlab.com/)
# ![image.png](attachment:image.png)

# In[ ]:




