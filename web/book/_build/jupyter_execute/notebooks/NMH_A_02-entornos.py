#!/usr/bin/env python
# coding: utf-8

# # Entornos (Ambientes de trabajo)

# Dado que Python y los paquetes que utilizamos en nuestro sistems operativo reciben actualizaciones y adicionalmente combinan el usos de unos con otros, es recomendable realizar un ambiente de trabajo. Este ambiente de trabajo tendrá las librerias y versiones necesarias para el buen funcionamiento de nuestras codes. A continuación se explica brevemente como crear un ambiente e instalar las librerias necesarias para el cursado. Se recuerda que para el curso se hará uso de Anaconda.
# 
# Corramos la siguiente linea de comando en nuestro Anaconda Prompt

# ```conda create --name nuevas_herramientas --file requirements.txt```

# Ya adentro del ambiente podemos instalar las librerias que se recomienda para el curso. Se debe descargar el archivo que se encuentra en el repositorio [requirements](https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria/-/blob/main/requirements.txt). Dentro del ambiente creado anteriormente tipear lo siguiente:

# De esta manera hemos creado un ambiente llamado 'nuevas_herramientas' al cual le hemos instalado una serie de paquetes. El paso siguiente será la activación del ambiente

# ```conda activate nuevas_herramientas```

# Ahora adentro del ambiente podemos seguir instalado cosas

# Para desactivar el entorno (ambiente) debe tipear la siguiente linea:

# ```conda deactivate```

# ### Para Windows users

# Para usuarios de windows se recomienda la instalación del paquete pipwin, para lo cual deben tipear lo siguiente:
# ```pip install pipwin```. Al no hacer esto podría traer aparejado problemas en la instalación de paquetes como GDAL, rasterio o geopandas. 
# Luego de instalado el ```pipwin```, instalar las librerías:
# ```pipwin install gdal```
# ```pipwin install rasterio```
# ```pipwin install geopandas```

# In[ ]:




