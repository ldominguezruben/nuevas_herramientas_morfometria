#!/usr/bin/env python
# coding: utf-8

# # Sobre como proyectar
# Este notebook pretende enseñar a usar una librería de proyección como es [pyproj](https://pyproj4.github.io/pyproj/stable/index.html). Esta librería permite proyectar información geoespacial a diferentes sistemas de proyección. Se sugiere la revisión de la página de [EPSG](https://epsg.io/).

# In[1]:


# importamos las librerias para el notebook
import pandas as pd
from pyproj import Proj, transform# projeccion
import folium


# In[2]:


# importamos un CSv con datos geograficos que tenemos
df = pd.read_csv('data/ucayali_tramo1.csv')

df.head(5)


# In[3]:


#Usamos folium para ploter la imagen
m = folium.Map(location=[df.iloc[1,11], df.iloc[1,12]],zoom_start=11, tiles='CartoDB positron')

for i, r in df.iterrows():
    lat = df.iloc[i,11]
    lon = df.iloc[i,12]
    folium.CircleMarker(location=[lat, lon]).add_to(m)
m


# In[4]:


# Ploteamos los datos de donde se realizo el test, en forma de linea para ver por donde se realizo el relevamiento
inProj = Proj(init='epsg:4326')#coordenadas geograficas WGS 84
outProj = Proj(init='epsg:32718')#UTM 16N

#Creamos lnos campos en la base de datos
df["XUTM16N"]=''
df["YUTM16N"]='' 

#proyectamos
df['XUTM16N'],df['YUTM16N']= transform(inProj,outProj,df.iloc[:,11],df.iloc[:,12])

df.head()


# In[5]:


import ee
import geemap
from geemap.algorithms import river


# In[6]:


Map = geemap.Map()
Map


# In[7]:


water_mask = ee.ImageCollection(
    "projects/sat-io/open-datasets/GRWL/water_mask_v01_01"
).median()
Map.addLayer(water_mask, {'min': 11, 'max': 125, 'palette': 'blue'}, 'GRWL Water Mask')


# In[8]:


grwl_water_vector = ee.FeatureCollection(
    "projects/sat-io/open-datasets/GRWL/water_vector_v01_01"
)
Map.addLayer(
    grwl_water_vector.style(**{'fillColor': '00000000', 'color': 'FF5500'}),
    {},
    'GRWL Vector',
)


# In[ ]:




