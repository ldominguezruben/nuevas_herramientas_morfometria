#!/usr/bin/env python
# coding: utf-8

# # Tarea 2
# Objetivo: que el alumno ejecute el paquete de códigos que se enseñaran en clase y que codifique su propio algoritmos para la determinación de variables morfométricas.

# ## Ejercicios
# **Ejercicio 1.** Extraiga una imagen promediada anualmente (o de una fecha en particular) usando los códigos Landsat 1, 3, 5, 7, 8 y Sentinel 2. Recuerde de usar una corrección de nubes menor al 10%. Tome pasos temporales de 5 años, desde la primera imagen hasta la última (si cuenta con datos sino extienda el periodo). Todo esto hagalo sobre el tramo de estudio y sobre una misma coloque secuencialmente las imagenes descargadas (Recuerde usar la misma escala). Puede implementar QGIS para el ejercicio.
# 
# **Ejercicio 2.** Haga un code usando la colección de [Water Ocurrence](https://developers.google.com/earth-engine/tutorials/tutorial_global_surface_water_02) (recuerde que es el ejemplo5_1 compartido en GEE), guíese para ello de los códigos provistos y obtenga una imagen final usando QGIS de su tramo de estudio.
# 
# **Ejercicio 3.** Ejecute el codigo GEEMAP para entender la tendencia de presencia de agua sobre su tramo de estudio. Opcional: Fijese en el code GEEMAP nombrado surface_water_mapping.ipynb.
# 
# **Ejercicio 4.** Vectorice una imagen RASTER obtenida de la plataforma GEE de su tramo de estudio.
# 
# **Ejercicio 5.** Ejecute la función _river_ (nuestro caso el compartido por GEE) y determine la línea central de su cauce. Evalue los métodos Jones (2019) y Zou et al (2018). Determine cual de los dos le ofrecen mejor precisión.
# 
# **Ejercicio 6.** Calcule el ancho de su tramo de cauce usando la aplicación de la función _river_. Evalue el resultado alcanzado y para ello calcule (manualmente) en al menos 30 secciones el ancho de las mismas compara y calcule parametros estadisticos como desvio estándar.
# 
# **Ejercicio 7.** En base a lo visto en la clase extraiga el .shp alcanzado del ejercicio 5, aplíquele un filtro para obtener una linea contínua (visto en la clase). De esta línea 'filtrada' calcule curvatura, determine curvas presentes, puntos de inflección, puntos de máxima curvatura y la sinuosidad.
# 

# In[ ]:




