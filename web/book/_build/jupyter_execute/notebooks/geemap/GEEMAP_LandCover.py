#!/usr/bin/env python
# coding: utf-8

# # GEEMAP
# Global Land Cover

# In[2]:


#!pip install geemap
import geemap


# ## Analizamos Global Land Cover

# In[3]:


Map = geemap.Map()
Map


# ### Agregamos info MODIS global land cover
# 
# MODIS MCD12Q1.006 Land Cover Type Yearly Global 500m 
# 
# https://developers.google.com/earth-engine/datasets/catalog/MODIS_006_MCD12Q1

# In[ ]:


landcover = ee.Image('MODIS/006/MCD12Q1/2013_01_01').select('LC_Type1')

vis = {
    'min': 1.0,
    'max': 17.0,
    'palette': [
        '05450a',
        '086a10',
        '54a708',
        '78d203',
        '009900',
        'c6b044',
        'dcd159',
        'dade48',
        'fbff13',
        'b6ff05',
        '27ff87',
        'c24f44',
        'a5a5a5',
        'ff6d4c',
        '69fff8',
        'f9ffa4',
        '1c0dff',
    ],
}

Map.setCenter(6.746, 46.529, 2)
Map.addLayer(landcover, vis, 'MODIS Land Cover')


# In[ ]:


Map.add_legend(builtin_legend='MODIS/006/MCD12Q1')


# In[ ]:


countries_shp = '../data/countries.shp'
countries = geemap.shp_to_ee(countries_shp)
Map.addLayer(countries, {}, 'Countries')


# In[ ]:


out_dir = os.path.join(os.path.expanduser('~'), 'Downloads')
global_stats = os.path.join(out_dir, 'global_stats.csv')

# statistics_type can be either 'SUM' or 'PERCENTAGE'
# denominator can be used to convert square meters to other areal units, such as square kilimeters
geemap.zonal_statistics_by_group(
    landcover,
    countries,
    global_stats,
    statistics_type='PERCENTAGE',
    denominator=1000000,
    decimal_places=2,
)


# In[ ]:


geemap.create_download_link(global_stats)


# In[ ]:


## Analyzing Global Land Cover

Map = geemap.Map()
Map

### Add MODIS global land cover data

MODIS MCD12Q1.006 Land Cover Type Yearly Global 500m 

https://developers.google.com/earth-engine/datasets/catalog/MODIS_006_MCD12Q1

landcover = ee.Image('MODIS/006/MCD12Q1/2013_01_01').select('LC_Type1')

vis = {
    'min': 1.0,
    'max': 17.0,
    'palette': [
        '05450a',
        '086a10',
        '54a708',
        '78d203',
        '009900',
        'c6b044',
        'dcd159',
        'dade48',
        'fbff13',
        'b6ff05',
        '27ff87',
        'c24f44',
        'a5a5a5',
        'ff6d4c',
        '69fff8',
        'f9ffa4',
        '1c0dff',
    ],
}

Map.setCenter(6.746, 46.529, 2)
Map.addLayer(landcover, vis, 'MODIS Land Cover')

Map.add_legend(builtin_legend='MODIS/006/MCD12Q1')

countries_shp = '../data/countries.shp'
countries = geemap.shp_to_ee(countries_shp)
Map.addLayer(countries, {}, 'Countries')

out_dir = os.path.join(os.path.expanduser('~'), 'Downloads')
global_stats = os.path.join(out_dir, 'global_stats.csv')

# statistics_type can be either 'SUM' or 'PERCENTAGE'
# denominator can be used to convert square meters to other areal units, such as square kilimeters
geemap.zonal_statistics_by_group(
    landcover,
    countries,
    global_stats,
    statistics_type='PERCENTAGE',
    denominator=1000000,
    decimal_places=2,
)

geemap.create_download_link(global_stats)

