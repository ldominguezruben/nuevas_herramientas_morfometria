#!/usr/bin/env python
# coding: utf-8

# # Interactive extraction of pixel values and interactive region reduction

# ## Interactive extraction of pixel values

# ### Import libraries

# In[1]:


import os
import ee
import geemap


# ### Create an interactive map

# In[2]:


Map = geemap.Map()
Map


# ### Add data to the map

# In[3]:


landsat7 = ee.Image('LE7_TOA_5YEAR/1999_2003').select([0, 1, 2, 3, 4, 6])
landsat_vis = {'bands': ['B4', 'B3', 'B2'], 'gamma': 1.4}
Map.addLayer(landsat7, landsat_vis, "LE7_TOA_5YEAR/1999_2003")

Map.set_plot_options(add_marker_cluster=True)


# ### Activate the plotting tool
# 
# Tick the `Plotting` checkbox and click the mouse on the map to start displaying charts.

# ### Export pixel values to shapefile/csv

# In[4]:


out_dir = os.path.join(os.path.expanduser('~'), 'Downloads')
# out_csv = os.path.join(out_dir, 'points.csv')
out_shp = os.path.join(out_dir, 'points.shp')


# In[5]:


Map.extract_values_to_points(out_shp)


# ## Interactive Region Reduction

# ### Import libraries

# In[6]:


import os
import ee
import geemap


# ### Create an interactive map

# In[7]:


m = geemap.Map()


# ### Add add to the map

# In[8]:


collection = (
    ee.ImageCollection('MODIS/006/MOD13A2')
    .filterDate('2015-01-01', '2019-12-31')
    .select('NDVI')
)

# Convert the image collection to an image.
image = collection.toBands()

ndvi_vis = {
    'min': 0.0,
    'max': 9000.0,
    'palette': [
        'FFFFFF',
        'CE7E45',
        'DF923D',
        'F1B555',
        'FCD163',
        '99B718',
        '74A901',
        '66A000',
        '529400',
        '3E8601',
        '207401',
        '056201',
        '004C00',
        '023B01',
        '012E01',
        '011D01',
        '011301',
    ],
}

m.addLayer(image, {}, 'MODIS NDVI Time-series')
m.addLayer(image.select(0), ndvi_vis, 'MODIS NDVI VIS')

m


# ### Set reducer

# In[9]:


m.set_plot_options(add_marker_cluster=True, marker=None)
m.roi_reducer = ee.Reducer.mean()


# ### Export data

# In[10]:


out_dir = os.path.join(os.path.expanduser('~'), 'Downloads')
out_csv = os.path.join(out_dir, 'points.csv')
out_shp = os.path.join(out_dir, 'ndvi.shp')
m.extract_values_to_points(out_shp)


# In[ ]:




