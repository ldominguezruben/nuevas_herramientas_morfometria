#!/usr/bin/env python
# coding: utf-8

# # Landsat 9

# Import libraries.

# In[2]:


import ee
import geemap


# Create an interactive map.

# In[3]:


Map = geemap.Map()


# Find out how many Landsat imgaes are available.

# In[4]:


collection = ee.ImageCollection('LANDSAT/LC09/C02/T1_L2')
print(collection.size().getInfo())


# Create a median composite.

# In[5]:


median = collection.median()


# Apply scaling factors. See https://developers.google.com/earth-engine/datasets/catalog/LANDSAT_LC08_C02_T1_L2#bands

# In[6]:


def apply_scale_factors(image):
    opticalBands = image.select('SR_B.').multiply(0.0000275).add(-0.2)
    thermalBands = image.select('ST_B.*').multiply(0.00341802).add(149.0)
    return image.addBands(opticalBands, None, True).addBands(thermalBands, None, True)


# In[7]:


dataset = apply_scale_factors(median)


# Specify visualization parameters.

# In[8]:


vis_natural = {
    'bands': ['SR_B4', 'SR_B3', 'SR_B2'],
    'min': 0.0,
    'max': 0.3,
}

vis_nir = {
    'bands': ['SR_B5', 'SR_B4', 'SR_B3'],
    'min': 0.0,
    'max': 0.3,
}


# Add data layers to the map.

# In[9]:


Map.addLayer(dataset, vis_natural, 'True color (432)')
Map.addLayer(dataset, vis_nir, 'Color infrared (543)')
Map


# ![](https://i.imgur.com/USPMXzw.png)

# Create linked maps for visualizing images with different band combinations. For more information on common band combinations of Landsat 8/9, see https://gisgeography.com/landsat-8-bands-combinations/

# Specify visualization parameters.

# In[10]:


vis_params = [
    {'bands': ['SR_B4', 'SR_B3', 'SR_B2'], 'min': 0, 'max': 0.3},
    {'bands': ['SR_B5', 'SR_B4', 'SR_B3'], 'min': 0, 'max': 0.3},
    {'bands': ['SR_B7', 'SR_B6', 'SR_B4'], 'min': 0, 'max': 0.3},
    {'bands': ['SR_B6', 'SR_B5', 'SR_B2'], 'min': 0, 'max': 0.3},
]


# Specify labels for each layers.

# In[11]:


labels = [
    'Natural Color (4, 3, 2)',
    'Color Infrared (5, 4, 3)',
    'Short-Wave Infrared (7, 6 4)',
    'Agriculture (6, 5, 2)',
]


# Create linked maps.

# In[12]:


geemap.linked_maps(
    rows=2,
    cols=2,
    height="400px",
    center=[40, -100],
    zoom=4,
    ee_objects=[dataset],
    vis_params=vis_params,
    labels=labels,
    label_position="topright",
)


# ![](https://i.imgur.com/c4FsGBI.png)

# Create a split-panel map for comparing Landsat 8 and 9 images.
# 
# Retrieve two sample images.

# In[13]:


landsat8 = ee.Image('LANDSAT/LC08/C02/T1_L2/LC08_015043_20130402')
landsat9 = ee.Image('LANDSAT/LC09/C02/T1_L2/LC09_015043_20211231')


# Apply scaling factors.

# In[14]:


landsat8 = apply_scale_factors(landsat8)
landsat9 = apply_scale_factors(landsat9)


# Generate Earth Engine layers.

# In[15]:


left_layer = geemap.ee_tile_layer(landsat8, vis_natural, 'Landsat 8')
right_layer = geemap.ee_tile_layer(landsat9, vis_natural, 'Landsat 9')


# Create a split-panel map.

# In[16]:


Map = geemap.Map()
Map.split_map(left_layer, right_layer)
Map


# ![](https://i.imgur.com/i6lUYHF.png)

# In[ ]:




