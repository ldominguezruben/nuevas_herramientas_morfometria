from numba import jit
import numpy as np

def lag(s, cur, t, LAG, LPRINT, WIDTH):
    """
    Impose a phase lag to the curvature signal by replacing the local 
    curvature with the upstream-wise moving averaged curvature.
    
    Parameters
    ----------
    s : ndarray 
        streamwise distance
    cur : ndarray
        curvature
    t : integer
        current # of time step
    
    Returns
    -------
    s : ndarray 
        streamwise distance with considering phase lag
    cur : ndarray
        curvature with considering phase lag

    """
    if LAG == 0:
        return cur
    else:
        num = int(WIDTH*LAG/np.mean(np.diff(s)))
    if np.mod(t, LPRINT) == 0:
        print('+> Adding phase lag to local curvature...', end='')
    length = cur.size
    cur0 = np.copy(cur)
    for i in range(2, length):
        M = i if i < num else num
        c = 0
        for j in range(M):
            c += (2/M-j*2/M/(M-1))*cur0[i-j]
        cur[i] = c
    if np.mod(t, LPRINT) == 0:
        print(' [done]')
    return cur