import numpy as np

def migrationRVR(s, x, y, cur_flt, cur_lag, theta, t, WIDTH, DEPTH, UB0, C0, FR0, CF0, E0, DT, TSTEPS, GPRINT, LPRINT):
    """
    Channel migration RVR Meander method
    Dominguez Ruben. FICH-UNL
    
    Parameters
    ----------
    s : ndarray 
        streamwise distance
    x : ndarray
        x coordinate of river centerline
    y : ndarray
        y coordinate of river centerline
    cur_flt : ndarray
        filtered curvature
    cur_lag : ndarray
        filtered curvature with considering phase lag
    theta : ndarray
        angular ampitude
    t : integer
        current # of time step
    
    Returns
    -------
    s : ndarray 
        streamwise distance after migration
    x : ndarray
        x coordinate of river centerline after migration
    y : ndarray
        y coordinate of river centerline after migration

    """
    if np.mod(t, LPRINT) == 0 and np.mod(t, GPRINT) != 0:
        print('\n[Paso de tiempo '+str(t)+'/'+str(TSTEPS)
              +']\n+> Corriendo RVRMeander adaptado...', end='')
    elif np.mod(t, LPRINT) == 0 and np.mod(t, GPRINT) == 0:
        print('\n[Paso de tiempo gráfico '+str(t)+'/'+str(TSTEPS)
              +']\n+> Corriendo RVRMeander adaptado...', end='')

    beta = WIDTH/2/DEPTH
    A = 3.8*(1+beta/6.96*np.exp(-6.96/beta))

    s = s/WIDTH
    x = x/WIDTH
    y = y/WIDTH
    cur_flt = cur_flt*WIDTH
    cur_lag = cur_lag*WIDTH

    chi = (np.sqrt((x[0]-x[-1])**2 + (y[0]-y[-1])**2)/s[-1])**(1/3)
    a1 = UB0*(2*np.random.random(1)[0] - 1) + chi*C0
    a2 = 2*CF0*beta*chi
    a3 = -chi
    a4 = CF0*beta*(chi**5*FR0**2 + (A+1)*chi**2 + 5*np.sqrt(CF0)*(A + chi**2*FR0**2))
    ub = a1*np.exp(-a2*s) + a3*cur_flt + a4*cur_lag 

    x += E0*ub*DT*np.sin(theta)
    y -= E0*ub*DT*np.cos(theta)

    x = x*WIDTH
    y = y*WIDTH
        
    for j in range(1, x.size):
       s[j] = np.sqrt((x[j]-x[j-1])**2 + (y[j]-y[j-1])**2) + s[j-1]
    if np.mod(t, LPRINT) == 0:
        print(' [done]')
  ##########################################
    import csv
    import pandas as pd

    if np.mod(t, GPRINT) == 0:
        file_name=str(t)+".csv"
        df = pd.DataFrame({"x" : x, "y" : y})
        df.to_csv(file_name, index=False)
        fole = str(t)+'OK_SAVE'
        print(fole)
  ##########################################
        return s, x, y
