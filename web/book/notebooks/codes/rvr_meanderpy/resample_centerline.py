import numpy as np

ZERO = 1e-8


def resample_centerline(s, x, y, INTERVAL):
    """
    Resample centerline coordinates.
    
    Parameters
    ----------
    s : ndarray 
        streamwise distance
    x : ndarray
        x coordinate of river centerline read from file
    y : ndarray
        y coordinate of river centerline read from file
    
    Returns
    -------
    sn : ndarray 
        streamwise distance after resampling
    xn : ndarray
        x coordinate of river centerline after resampling
    yn : ndarray
        y coordinate of river centerline after resampling

    """
    N = int(s[-1]//INTERVAL) + 1
    ds = s[-1]/N
    length = s.size
    xn = np.zeros(N+1)
    yn = np.zeros(N+1)
    xn[0], yn[0], xn[-1], yn[-1] = x[0], y[0], x[-1], y[-1]
    for i in range(1, N):
        curr_loc = i*ds
        for j in range(length-1):
            if s[j] < curr_loc and s[j+1] > curr_loc:
                r = (curr_loc-s[j])/(s[j+1]-s[j])
                break
        xn[i] = x[j] + (x[j+1]-x[j])*r
        yn[i] = y[j] + (y[j+1]-y[j])*r
    sn = np.zeros(xn.size)
    for i in range(1,xn.size):
        sn[i] = sn[i-1] + np.sqrt((xn[i]-xn[i-1])**2 + (yn[i]-yn[i-1])**2)
    curn, thetan = tan2curv(sn, xn, yn)
    return sn, xn, yn, curn, thetan

def tan2curv(s, x, y):
    """
    Compute curvature using 'arctan2' method.
    
    Parameters
    ----------
    s : ndarray 
        streamwise distance
    x : ndarray
        x coordinate of river centerline
    y : ndarray
        y coordinate of river centerline
    
    Returns
    -------
    cur : ndarray
        curvature
    forw : ndarray
        angular ampitude

    """
    length = x.size
    cur = np.zeros(length)
    forw = np.zeros(length)
    back = np.zeros(length)
    for i in range(1, length-1):
        forw[i] = np.arctan2(y[i+1]-y[i], x[i+1]-x[i])
        back[i] = np.arctan2(y[i]-y[i-1], x[i]-x[i-1])
        angle_atan2 = forw[i] - back[i]
        cur[i] = angle_atan2/(s[i+1]-s[i-1])*2
        if np.abs(cur[i]) < ZERO:
            cur[i] = 0
    for i in range(1, length-1):
        ave = (cur[i-1]+cur[i+1])/2
        if np.abs(cur[i]-ave) > 5*np.abs(cur[i-1]-cur[i+1]):
            cur[i] = ave
    forw[0], forw[-1] = back[1], forw[-2]
    return cur, forw