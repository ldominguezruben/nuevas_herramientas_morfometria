from scipy.signal import savgol_filter
import numpy as np

def smooth_centerline(x, y, SMOLEV):
    """
    Smooth centerline using Savitzky–Golay filter 
    (5-point quadratic polynomial method).
    Dominguez Ruben.FICH-UNL
    
    Number of passes is equal to:
        SMOOTHINGLEVEL (SMOOTHINGLEVEL < 39)
        int(1.1^SMOOTHINGLEVEL) (SMOOTHINGLEVEL >= 39)
    
    Parameters
    ----------
    x : ndarray
        x coordinate of river centerline before smoothing
    y : ndarray
        y coordinate of river centerline before smoothing
	SMOKEV : 
    
    Returns
    -------
    s : ndarray 
        streamwise distance after smoothing
    x : ndarray
        x coordinate of river centerline after smoothing
    y : ndarray
        y coordinate of river centerline after smoothing
    
    """
    n = SMOLEV if SMOLEV < 39 else int(np.around(1.1**SMOLEV,decimals=0))
    xa, xb, ya, yb = x[0], x[-1], y[0], y[-1]
    for i in range(n):
        x = savgol_filter(x, 5, 2, mode='nearest')
        y = savgol_filter(y, 5, 2, mode='nearest')
        x[0], x[-1], y[0], y[-1] = xa, xb, ya, yb
    s = np.zeros(x.size)
    for i in range(1, x.size):
        s[i] = s[i-1] + np.sqrt((x[i]-x[i-1])**2 + (y[i]-y[i-1])**2)
    return s, x, y